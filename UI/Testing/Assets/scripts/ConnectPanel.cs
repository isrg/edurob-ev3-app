﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ConnectPanel : MonoBehaviour
{
	public static ConnectPanel instance;

	public InputField inputField;
	public Button testConnectionButton;
	public Button forgetIPButton;
	public Text outputText;
	public GameObject LoginPanel;

	string ipAddress;

	void Start()
	{
		instance = this;

		ipAddress = PlayerPrefs.GetString("robotip");
		inputField.text = ipAddress;
	}

	public void InputFieldChanged()
	{
		var s = inputField.text;

		if (Extensions.IsIPv4(s))
		{
			ipAddress = s;
			PlayerPrefs.SetString("robotip", s);
			testConnectionButton.interactable = true;
		}
		else
			testConnectionButton.interactable = false;

	}

	NaoConnector naoConnector;

	public void TestConnection()
	{
		Print(Level.Info, "ConnectPanel: Trying to connect to " + ipAddress);
		//AsynchronousClient.StartClient();

		//SynchronousSocketClient.StartClient();

		//SendData.instance.UpdateOutput("Attempt " + ++n, MessageType.Info);

		//SendData.instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"animationHello"} );
		naoConnector = NaoConnector.instance.Init(ipAddress, 9559);
		naoConnector.TryToConnect();
		//		SendData.instance.Send("TestData", "WooHoo");
	}

	public void TestCommand()
	{
		var cmd = "animationHello";
		Print(Level.Info, "ConnectPanel: Trying to send command {\"" + cmd + "\"} to " + ipAddress);
		//AsynchronousClient.StartClient();

		//SynchronousSocketClient.StartClient();

		//SendData.instance.UpdateOutput("Attempt " + ++n, MessageType.Info);

		if (naoConnector == null)
			naoConnector = NaoConnector.instance.Init(ipAddress, 9559);

		bool success = naoConnector.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"animationHello"} );

		Print(Level.Info, "ConnectPanel:  command {\"" + cmd + "\"} result: " + success);
		//		naoConnector.TryToConnect();
		//		SendData.instance.Send("TestData", "WooHoo");
	}

	public void TestSSHStart()
	{
		if (naoConnector == null)
			naoConnector = NaoConnector.instance.Init(ipAddress, 9559);

		var session = naoConnector.TestSSHServerStart(ipAddress);
		if (session != null)
			session.disconnect();
	}

	public void Reset_can_send()
	{
		SendData.instance.dataRecv = "Y";
	}

	public void ForgetIPAddress()
	{
		ipAddress = string.Empty;
		PlayerPrefs.DeleteKey("robotip");
		inputField.text = string.Empty;
	}

	public GameObject ShowLoginDialog()
	{
		LoginPanel.SetActive(true);

		return LoginPanel;
	}

	public static void Print(Level level, string message)
	{
		instance.outputText.text += level + ": " + message + "\n";
		switch (level)
		{
			case Level.Info: Debug.Log(message); break;
			case Level.Warn: Debug.LogWarning(message); break;
			case Level.Error: Debug.LogError(message); break;
		}
	}
}

public enum Level
{
	Info,
	Warn,
	Error
}
