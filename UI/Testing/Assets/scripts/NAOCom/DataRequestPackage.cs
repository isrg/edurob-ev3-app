﻿using System;

[Serializable]
public class DataRequestPackage
{
	/**
 * Command to execute
 */
	public NAOCommands command;
	public string[] commandArguments;

	/**
 * Constructor
 */
	public DataRequestPackage()
	{
		command = NAOCommands.SYS_GET_INFO;
		commandArguments = new string[0];
	}


	public DataRequestPackage(NAOCommands aCommand, string[] aArgs)
	{
		command = aCommand;
		commandArguments = aArgs;
	}

	public String toString()
	{
		string ret = "" + command;
		foreach (string s in commandArguments)
		{
			ret += "\n\t" + s;
		}		

		return ret;
	}
}
