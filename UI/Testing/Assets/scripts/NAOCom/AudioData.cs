﻿using System;

public class AudioData {

	public int masterVolume;
	public float playerVolume;
	public float speechVolume;
	public string speechVoice;
	public string speechLanguage;
	public string[] speechLanguagesList;
	public string[] speechVoicesList;
	public float speechPitchShift;
	public float speechDoubleVoice;
	public float speechDoubleVoiceLevel;
	public float speechDoubleVoiceTimeShift;

	public String toString(){
		String ret = "\tmasterVolume: " + masterVolume
			+ "\n\tplayerVolume: " + playerVolume
			+ "\n\tspeechVolume: " + speechVolume
			+ "\n\tspeechVoice: " + speechVoice
			+ "\n\tspeechLanguage: " + speechLanguage
			+ "\n\tspeechLanguages: ";

		foreach (string lang in speechLanguagesList )
		{
			ret += "\n\t\t" + lang;
		}

		ret += "\n\tspeechVoices: ";
		foreach (string voice in speechVoicesList )
		{
			ret += "\n\t\t" + voice;
		}

		ret += "\n\tspeechPitchShift: " + speechPitchShift
			+ "\n\tspeechDoubleVoice: " + speechDoubleVoice
			+ "\n\tspeechDoubleVoiceLevel: " + speechDoubleVoiceLevel
			+ "\n\tspeechDoubleVoiceTimeShift: " + speechDoubleVoiceTimeShift;

		return ret;
	}

}

