﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System;
using System.IO;
using System.Threading;

using Tamir.SharpSsh.jsch;
using Tamir.SharpSsh.java.io;
using Tamir.SharpSsh;


/**
 * SharpSsh sources:
 * 1. http://sourceforge.net/projects/sharpssh/ -- Fails to load in Unity - SpritePacker failed to get types.. The classes in the module cannot be loaded. UnityEditor.Sprites.Packer:GetSelectedPolicyId()
 * 2. https://bitbucket.org/mattgwagner/sharpssh -- Currently in use
 * 3. https://ketulpatel.wordpress.com/2010/05/13/enhanced-sharpssh/ -- not tried yet
 **/


public class NaoConnector : MonoBehaviour
{
	public static NaoConnector instance;

	public static readonly string defaultHost = "192.168.0.100";
	public static readonly int defaultPort = 5050;
	public static readonly int defaultReadTimeout = 3000;
	public static readonly int minReadTimeout = 100;
	public static readonly int connectionMaxTries = 3;
	public static readonly int connectionMaxTimeouts = 30;
	static readonly string SSH_COMMAND_SERVER_START = "naocom/start.sh";
	static readonly string SSH_CHANNEL_EXEC = "exec";
	static readonly String SSH_CHANNEL_SFTP = "sftp";
	public static readonly String PREFERENCES_SSH_USER = "ssh_default_usr";
	public static readonly String PREFERENCES_SSH_PASSWORD = "ssh_default_pw";


	List<string> hostAdresses = new List<string>();
	int port = defaultPort;
	bool mUseCustomLoginData = false;
	string mSSHUser = "nao";
	string mSSHPassword = "nao";
	List<INetworkDataRecievedListener> dataRecievedListener = new List<INetworkDataRecievedListener>();
	ConnectionState state = ConnectionState.CONNECTION_INIT;
	string host;
	Socket socket = null;
	//BufferedReader streamIn = null;
	//OutputStream streamOut = null;
	bool stop = false;
	int timeoutCounter = 0;
	JSch mSSH = new JSch();

	/**
	 * Constructor
	 * @param aHost
	 * @param aPort
	 */
	public NaoConnector Init(string aHost, int aPort) {
		hostAdresses.Add(aHost);
		port = aPort;

		return this;
	}

	/**
	 * Constructor
	 * @param connector	{@link NAOConnector} to copy settings (host adresses and port) from.
	 */
	public NaoConnector Init(NaoConnector connector) {
		hostAdresses = connector.hostAdresses;
		port = connector.port;
		mUseCustomLoginData = connector.mUseCustomLoginData;
		mSSHUser = connector.mSSHUser;
		mSSHPassword = connector.mSSHPassword;
		dataRecievedListener = connector.dataRecievedListener;

		return this;
	}

	/**
	 * Try to connect to one of the available remote addresses.
	 * @return	{@code true} if successful, {@code false} otherwise.
	 */
	private bool connect()
	{
		state = ConnectionState.CONNECTION_INIT;

		// Data buffer for incoming data.
		byte[] bytes = new byte[1024];

		foreach ( string h in hostAdresses )
		{
			try
			{
				host = h;
				// create socket
				socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
				socket.ReceiveTimeout = defaultReadTimeout;

				socket.Connect(h, port);

				//socket = new Socket( InetAddress.getByName(host).getHostAddress(), port ); 
				//socket.setSoTimeout(defaultReadTimeout);
				// //streamIn = new BufferedReader( new InputStreamReader( new BufferedInputStream(socket.getInputStream()) ) );
				// //streamOut = socket.getOutputStream();		

				// try to connect
				int tries = connectionMaxTries;
				while (!stop && socket != null && state == ConnectionState.CONNECTION_INIT && tries > 0)
				{
					// send connection request
					DataRequestPackage p = new DataRequestPackage(NAOCommands.SYS_CONNECT, new string[0]);
					string data = JsonUtility.ToJson(p);
					ConnectPanel.Print(Level.Info, "NaoConnector: Sent string: " + data);

					byte[] msg = Encoding.Default.GetBytes(data);
					//byte[] msg = Encoding.ASCII.GetBytes("This is a test<EOF>");

					// Send the data through the socket.
					int bytesSent = socket.Send(msg);

					////-------------- WAIT FOR DATA ---------------////

					// Receive the response from the remote device.
					int bytesRec = socket.Receive(bytes);
					data = Encoding.Default.GetString(bytes, 0, bytesRec);
//
//					var d0 =  Encoding.Default.GetString(bytes, 0, bytesRec);
//					var d1 =  Encoding.ASCII.GetString(bytes, 0, bytesRec);
//					var d2 =  Encoding.UTF7.GetString(bytes, 0, bytesRec);
//					var d3 =  Encoding.UTF8.GetString(bytes, 0, bytesRec);
//					ConnectPanel.Print(Level.Info, "NaoConnector: Responses - " + d0 + " (default), " + d1 + " (ASCII), " + d2 + " (UTF7), " + d3 + " (UTF8)");

					//ConnectPanel.Print(Level.Info, "NaoConnector: received " + data);

					DataResponsePackage response = JsonUtility.FromJson<DataResponsePackage>(data);

					if (response.request.command == NAOCommands.SYS_CONNECT && response.requestSuccessfull)
					{
						state = ConnectionState.CONNECTION_ESTABLISHED;
						notifyDataRecievedListeners(response);
						return true;
					}

					tries--;
				}

			}
//			catch (UnknownHostException e)
//			{
//				state = ConnectionState.CONNECTION_UNKNOWN_HOST;
//				ConnectPanel.Print(Level.Info, "NAOConnector: Host unknown " + host);

//				MainActivity.getInstance().runOnUiThread( new Runnable()
//					{				
//					@Override
//					public void run() {
//						Toast.makeText(MainActivity.getInstance().getApplicationContext(),
//							R.string.net_unknown_host, Toast.LENGTH_SHORT).show();
//					}
//				});
//			}
			catch (IOException e)
			{
				state = ConnectionState.CONNECTION_ESTABLISHED_FAILED;
				ConnectPanel.Print(Level.Warn, "NaoConnector: IO Exception on connnection with " + host + ":" + port);
				ConnectPanel.Print(Level.Warn, "NaoConnector: IO Exception:" + e.Message);
			}
			catch (Exception e)
			{
				ConnectPanel.Print(Level.Warn, "NaoConnector: Exception: " + e.Message + "\n" + e.StackTrace);
			}
		}

		return false;
	}
		
	public void notifyDataRecievedListeners(DataResponsePackage data)
	{
		foreach (INetworkDataRecievedListener listener in dataRecievedListener )
		{
//			Runnable r = new NetworkDataRecievedListenerNotifier(listener, data);
//			new Thread(r).start();
			ConnectPanel.Print(Level.Info, "NaoConnector: notifyDataRecievedListeners - " + data.request);
		}
	}


	/**
	 * Function if thread started
	 */
	public void TryToConnect() {

		bool vRetry = true;
		bool vTriedSsh = false;

		while (vRetry)
		{
			// try to connect
			vRetry = false;
			if (connect())
			{

				// connected
				ConnectPanel.Print(Level.Info, "NaoConnector: Connected!");

				// set new connection timeout
				try
				{
					socket.ReceiveTimeout = minReadTimeout;
				}
				catch (SocketException e)
				{
					ConnectPanel.Print(Level.Info, "NaoConnector: " + e.Message);
				}

				// read data
				while ( !stop && socket != null && state == ConnectionState.CONNECTION_ESTABLISHED )
				{
					readData();
				}

				// try to disconnect
				if (disconnect())
				{
					ConnectPanel.Print(Level.Info, "NaoConnector: Disconnected");
				}
			}
			else
			{
				// try to start server using ssh and try to to connect again.
				if (!vTriedSsh && sshServerStart())
				{
					vRetry = true;
					vTriedSsh = true;
				}
				else
				{
					ConnectPanel.Print(Level.Warn, "NaoConnector: Establishing connection to " + host + ":" + port + " failed.");
				}
			}
		}
		// try to disconnect
		disconnect();

	}

	/**
	 * Read data until thread gets stopped or connection gets an error
	 */
	private void readData()
	{
		// Data buffer for incoming data.
		byte[] bytes = new byte[1024];

		// handle socket_CLOSED
		try
		{
			// Receive the response from the remote device.
			int bytesRec = socket.Receive(bytes);
			string data = Encoding.UTF8.GetString(bytes, 0, bytesRec);
			if (data != null)
			{
				timeoutCounter = 0;
				DataResponsePackage p = JsonUtility.FromJson<DataResponsePackage>(data);
				notifyDataRecievedListeners(p);
			}
			else
			{
				timeoutCounter++;
				if (timeoutCounter >= connectionMaxTimeouts )
				{
					ConnectPanel.Print(Level.Info, "NaoConnector: Timed out " + timeoutCounter + " times.");
					stopConnector();
				}
			}
		}
//		catch( SocketTimeoutException e )
//		{
//		}
		catch (IOException e)
		{
			ConnectPanel.Print(Level.Error, "NaoConnector: IOException on connnection with " + host + ":" + port + "\n" + e.Message);
			state = ConnectionState.CONNECTION_ERROR;
		}
		catch (Exception e )
		{
			ConnectPanel.Print(Level.Error, "NaoConnector: " + e.Message);
		}

	}

	public void stopConnector()
	{
		stop = true;
	}

	/**
	 * Try to disconnect.
	 * @return	{@code true} if successful, {@code false} otherwise.
	 */
	private bool disconnect()
	{
		// Data buffer for incoming data.
		byte[] bytes = new byte[1024];

		try
		{
			// create disconnect request
			DataRequestPackage p = new DataRequestPackage(NAOCommands.SYS_DISCONNECT, new String[0]);

			String data = JsonUtility.ToJson(p);

			byte[] msg = Encoding.UTF8.GetBytes(data);
			//byte[] msg = Encoding.ASCII.GetBytes("This is a test<EOF>");

			// Send the data through the socket.
			int bytesSent = socket.Send(msg);

			////-------------- WAIT FOR DATA ---------------////

			// Receive the response from the remote device.
			int bytesRec = socket.Receive(bytes);
			data = Encoding.UTF8.GetString(bytes, 0, bytesRec);

			// analyse request
			DataResponsePackage response = JsonUtility.FromJson<DataResponsePackage>(data);
			if (response.request.command == NAOCommands.SYS_DISCONNECT && response.requestSuccessfull)
			{
				notifyDataRecievedListeners(response);

//				// Close connections
//				in.close();
//				out.close();
//				socket.close();
//				socket = null;

				// Release the socket.
				socket.Shutdown(SocketShutdown.Both);
				socket.Close();

				state = ConnectionState.CONNECTION_CLOSED;

				return true;
			}

		}
		catch (SocketException e)
		{
			ConnectPanel.Print(Level.Error, "NaoConnector: " + e.Message);
		}
		catch(IOException e)
		{
			ConnectPanel.Print(Level.Warn, "NaoConnector: IOException on connnection with " + host + ":" + port + "\n" + e.Message);
			state = ConnectionState.CONNECTION_ERROR;
		}
		catch (NullReferenceException e )
		{
			// no response on socket
			ConnectPanel.Print(Level.Warn, "NaoConnector: NullReferenceException on disconnect from " + host + ":" + port + "\n" + e.Message);
			notifyDataRecievedListeners( new DataResponsePackage(new DataRequestPackage(NAOCommands.SYS_DISCONNECT, new String[]{}), true) );
			state = ConnectionState.CONNECTION_CLOSED;
			return true;
		}

		return false;
	}

	public Session TestSSHServerStart(string hostName)
	{
		connectSSH();

		//		foreach ( string h in hostAdresses )
		//		{
		//			host = h;
		//			SshShell ssh = new SshShell(host, mSSHUser, mSSHPassword);
		//			try
		//			{
		//				ssh.Connect();
		//				ssh.
		//			}
		//			catch(JSchException e)
		//			{
		//				ConnectPanel.Print(Level.Error, "NaoConnector: " + e.Message);
		//			}
		//			catch (IOException e)
		//			{
		//				ConnectPanel.Print(Level.Error, "NaoConnector: " + e.Message);
		//			}
		//			ConnectPanel.Print(Level.Info, "Connected: " + ssh.Connected + " user: " + ssh.Username);
		//			ssh.Close();
		//		}
	}


	/**
	 * Tries to start remote server using ssh.
	 * @return {@code true} if server started, {@code false} otherwise.
	 */
	private bool sshServerStart()
	{
		ConnectPanel.Print(Level.Info, "NaoConnector: sshStartServer");
//		// show message
//		MainActivity.getInstance().runOnUiThread( new Runnable()
//			{				
//			@Override
//			public void run()
//				{
//				Toast.makeText(MainActivity.getInstance().getApplicationContext(),
//					R.string.net_try_server_start, Toast.LENGTH_SHORT).show();
//			}
//		});	

		// send command
		string[] vCommands = new string[]{ SSH_COMMAND_SERVER_START };
		if (sendSSHCommands(vCommands).Count > 0 )
		{
			// wait a few seconds for server to start
			try
			{
				
				Thread.Sleep(5000);
			}
			catch (ThreadInterruptedException e)
			{
				ConnectPanel.Print(Level.Info, "NaoConnector: " + e.Message);
			}

			return true;
		}

		return false;
	}

	public Dictionary<string, int> sendSSHCommands(string[] aCommands, params string[] aInput)
	{
		// Connect to ssh server
		Session vSession = connectSSH();
		Dictionary<string, int> vExitStatus = new Dictionary<string, int>();

		if( vSession != null)
		{
			// execute commands
			for (int i=0; i < aCommands.Length; i++)
			{
				string cmd = aCommands[i];
				try
				{
					// open channel
					Channel vChannel = vSession.openChannel(SSH_CHANNEL_EXEC);
					ChannelExec vChannelExec = (ChannelExec) vChannel;
					var vOutStream = vChannel.getOutputStream();
					vChannelExec.setCommand(cmd);
					vChannelExec.setOutputStream(Console.OpenStandardOutput());
					vChannelExec.setErrStream(Console.OpenStandardError());

					// connect
					ConnectPanel.Print(Level.Info, "NaoConnector: sending " + cmd);
					vChannel.connect();

					// get user information
					string vUser = PlayerPrefs.GetString(PREFERENCES_SSH_USER, "nao");
					string vPassword = PlayerPrefs.GetString(PREFERENCES_SSH_PASSWORD, "nao");
					if (mUseCustomLoginData)
					{
						vUser = mSSHUser;
						vPassword = mSSHPassword;
					}

					// send input
					if (i < aInput.Length)
					{
						// replace tags
						aInput[i] = aInput[i].Replace("%%USR%%", vUser);
						aInput[i] = aInput[i].Replace("%%PW%%", vPassword);

						ConnectPanel.Print(Level.Info, "NaoConnector: writing " + aInput[i]);
						//vOutStream.Write( (aInput[i]+"\n").getBytes() );


						byte[] msg = Encoding.Default.GetBytes(aInput[i]+"\n");
						int l = msg.Length;

						vOutStream.Write(msg, 0, l);
						//vOutStream.Write( (aInput[i]+"\n"));
						vOutStream.Flush();
					}

					// wait for command to complete
					while (!vChannel.isClosed())
					{
						try
						{
							Thread.Sleep(100);
						} catch (ThreadInterruptedException e)
						{
							ConnectPanel.Print(Level.Info, e.Message);
						}
					};

					// add exit status
					vExitStatus.Add( cmd, vChannel.getExitStatus() );
					vOutStream.Close();
					vChannel.disconnect();

				}
				catch(JSchException e)
				{
					ConnectPanel.Print(Level.Error, "NaoConnector: " + e.Message);
				}
				catch (IOException e)
				{
					ConnectPanel.Print(Level.Error, "NaoConnector: " + e.Message);
				}
			}
			// close ssh connection
			closeSSH(vSession);
		}

		return vExitStatus;
	}

	/**
	 * Connecting to ssh server
	 * @return			{@code true} if successful connected, {@code false} otherwise.
	 */
	Session connectSSH()
	{
		foreach (string hst in hostAdresses )
		{
			try
			{
				// get user information
				String vUser = PlayerPrefs.GetString(PREFERENCES_SSH_USER, "nao");
				String vPassword = PlayerPrefs.GetString(PREFERENCES_SSH_PASSWORD, "nao");
				if (mUseCustomLoginData)
				{
					vUser = mSSHUser;
					vPassword = mSSHPassword;
				}

				// create session
				//Session vSession = mSSH.getSession(vUser,  InetAddress.getByName(host).getHostAddress().toString(), 22 );
				Session vSession = mSSH.getSession(vUser, hst, 22);
				vSession.setPassword(vPassword);

				// avoid asking for key auth
				Hashtable vProperties = new Hashtable();
				vProperties.Add("StrictHostKeyChecking", "no");
				vSession.setConfig(vProperties);

				try
				{
					// connect to ssh server
					vSession.connect();
				}
				catch (JSchException err)
				{
					ConnectPanel.Print(Level.Warn, err.Message + " " + vSession.getUserName());
					if (err.Message.Contains("Auth fail"))
					{
						// ask for custom login data
						if (askForCustomLoginData())
						{
							vSession = connectSSH();
						}
						else
						{
							return null;
						}
					}
					else
					{
						ConnectPanel.Print(Level.Error, "NaoConnector: EXCEPTION: " + err.Message);
					}
				}
				return vSession;
			}
//			catch (UnknownHostException e)
//			{
//				ConnectPanel.Print(Level.Info, "NaoConnector: " +e.StackTrace);
//			}
			catch (JSchException e)
			{
				ConnectPanel.Print(Level.Warn, "NaoConnector: " + e.StackTrace);
			}
			catch (Exception e)
			{
				ConnectPanel.Print(Level.Warn, "NaoConnector: " +e.StackTrace);
			}
		}
		return null;
	}

	/**
	 * Function to aks user for custom login data.
	 * @return	{@code true} if new custom login data available, {@code false} otherwise.
	 */
	bool askForCustomLoginData()
	{
		ConnectPanel.Print(Level.Warn, "NaoConnector: Custom login not implemented");
		// show dialog
		ConnectPanel.instance.ShowLoginDialog();
		return true;
	}

	public void customLoginDataUpdated()
	{
		mUseCustomLoginData = true;
	}


	/**
	 * Closes SSH connection.
	 * @param aSession	{@link Session} to close.
	 * @return			{@code true} if successful, {@code false} otherwise.
	 */
	bool closeSSH(Session aSession)
	{		
		if (aSession != null)
		{
			aSession.disconnect();
			return true;
		}

		return false;
	}

	/**
	 * Send {@link NAOCommands} to remote NAO.
	 * @param aCommand	{@link NAOCommands} to send.
	 * @param aArgs		Array of {@link String} arguments to for {@code aCommand}.
	 * @return			{@code true} if successful, {@code false} otherwise.
	 */
	public bool sendCommand(NAOCommands aCommand, String[] aArgs)
	{
		// Data buffer for incoming data.
		byte[] bytes = new byte[1024];

//		if (state == ConnectionState.CONNECTION_ESTABLISHED)
//		{
			try
			{
				DataRequestPackage p = new DataRequestPackage(aCommand, aArgs);
				string data = JsonUtility.ToJson(p);


				ConnectPanel.Print(Level.Info, "NaoConnector: Sent string: " + data);

				byte[] msg = Encoding.UTF8.GetBytes(data);
				//byte[] msg = Encoding.ASCII.GetBytes("This is a test<EOF>");

				// Send the data through the socket.
				int bytesSent = socket.Send(msg);

				////-------------- WAIT FOR DATA ---------------////

				// Receive the response from the remote device.
				int bytesRec = socket.Receive(bytes);
				ConnectPanel.Print(Level.Info, "NaoConnector: Received bytes: " + bytes.GetLength(0) + " " + bytes.Length);
				data = Encoding.UTF8.GetString(bytes, 0, bytesRec);
				DataResponsePackage response = JsonUtility.FromJson<DataResponsePackage>(data);

//				out.write(data.getBytes());
				return true;
			}
			catch(IOException err)
			{
				ConnectPanel.Print(Level.Warn, "NaoConnector: IOException on sending " + aCommand + " to " + host + ":" + port + "\n" + err.Message);
				stopConnector();
//			}
//		}
//		else
//		{
//			ConnectPanel.Print(Level.Warn, "NaoConnector: Could not connect when sending " + aCommand + " to " + host + ":" + port + "\nConnection state: " + state );
		}

		return false;
	}

	void Awake()
	{
		instance = this;
	}

	void Start()
	{
		
	}
	
	void Update()
	{
		
	}
}
