﻿using System;

public class NetworkDataReceivedListenerNotifier
{
	private DataResponsePackage data;
	private INetworkDataRecievedListener listener = null;

	public void NetworkDataRecievedListenerNotifier(INetworkDataRecievedListener aListener, DataResponsePackage aData)
	{
		data = aData;
		listener = aListener;
	}
		
	public void run()
	{
		listener.onNetworkDataRecieved(data);
	}

}