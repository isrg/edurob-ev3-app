﻿using System;
using System.Collections.Generic;

public class StiffnessData {

	private bool leftHandOpen = false;
	private bool rightHandOpen = false;
	private Dictionary<NAOJoints, float> jointStiffness = new Dictionary<NAOJoints, float>();

	/**
	 * @param joint {@link NAOJoints}
	 * @return {@link Float} between 0.0 (no stiffness) and 1.0 (full stiffness) of the {@code joint}
	 */
	public float getStiffness(NAOJoints joint)
	{
		if (jointStiffness.ContainsKey(joint))
		{
			return jointStiffness[joint];
		}

		return 0.0f;
	}

	public String toString()
	{
		String ret = "";
		foreach (NAOJoints joint in jointStiffness.Keys)
		{
			ret += "\t" + joint + ":" + jointStiffness[joint] + "\n";
		}
		return ret;
	}

	/**
	 * @return {@link Map} of {@link NAOJoints} and their stiffnesst values as {@link Float}.
	 */
	public Dictionary<NAOJoints, float> getJointStiffness()
	{
		return jointStiffness;
	}

	/**
	 * @return	{@code true} if NAO left hand is opened
	 */
	public bool isLeftHandOpen()
	{
		return leftHandOpen;
	}

	/**
	 * @return	{@code true} if NAO right hand is opened
	 */
	public bool isRightHandOpen()
	{
		return rightHandOpen;
	}

}
