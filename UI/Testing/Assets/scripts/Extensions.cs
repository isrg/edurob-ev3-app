﻿using System;

public class Extensions

{
	public static bool IsIPv4(string value)
	{
		var quads = value.Split('.');

		if (quads.Length != 4) return false;

		foreach(var quad in quads) 
		{
			int q;
			// if parse fails 
			// or length of parsed int != length of quad string (i.e.; '1' vs '001')
			// or parsed int < 0
			// or parsed int > 255
			// return false
			if (!Int32.TryParse(quad, out q) 
				|| !q.ToString().Length.Equals(quad.Length) 
				|| q < 0 
				|| q > 255) { return false; }

		}

		return true;
	}
}

