﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System;

public class SendData : MonoBehaviour
{
	static SendData _instance;

	public static SendData instance 
	{ 
		get
		{
			if(_instance == null)
			{
				_instance = GameObject.FindObjectOfType<SendData>();

				// Tell unity not to destroy this object when loading a 
				// new scene
				DontDestroyOnLoad(_instance.gameObject);
			}

			return _instance;
		}
	}

	// When the object awakens, we assign the instance variable.
	void Awake ()
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this.gameObject);
		}
	}

	int port = 9559;
	bool can_send = true;
	public string dataRecv = "Y";
	string message;
	string dataToSend;
	TcpClient client;
	byte[] messageData;
	//	byte[] recvData;
	//	Stream stream;
	//	NetworkStream stream;

	void Update() 
	{
		if (dataRecv != null) 
		{
			can_send = (dataRecv == "Y");

		}
	}

	public void Send (string data, string ifsuccess) 
	{    
		UpdateOutput("Value of can_send: " + can_send, MessageType.Info);

		// If the last answer from the server in the robot has been "N",
		// indicating that the robot is busy, the client won't send any data.
		if (can_send)
		{
			var ipAddress = PlayerPrefs.GetString("robotip");

			can_send = false;
			dataRecv = "N";

			try 
			{
				dataToSend = data;

				// A client for the server IP and the port 9558 is stablished.
				client = new TcpClient(AddressFamily.InterNetwork);
				client.Connect(ipAddress, port);
				UpdateOutput("Completed: client.Connect(" + ipAddress + ", " + port + ")", MessageType.Info);
				// The data is encoded.
				messageData = System.Text.Encoding.UTF8.GetBytes(dataToSend);

				var recvData = new byte[client.ReceiveBufferSize];

				// And written to the socket.
				NetworkStream stream = client.GetStream();
				//				stream = client.GetStream();
				stream.Write(messageData, 0, messageData.Length);

				UpdateOutput("Data written", MessageType.Info);

				// If everything goes well we will receive two answers, one
				// containing "N" indicating that the robot is already
				// preforming an activity and can't receive new data, and
				// a second one containing "Y" indicating that new data can
				// be sent to the robot.
				stream.BeginRead (recvData, 0, 1, ReadCallback, stream);

				UpdateOutput("recvData: " + recvData, MessageType.Info);

				stream.BeginRead (recvData, 0, 1, ReadCallback, stream);

				UpdateOutput("recvData: " + recvData, MessageType.Info);

				UpdateOutput("dataRecv: " + dataRecv, MessageType.Info);

				// If the data has been successfully sent the pop-up message 
				// will be the text in the ifsuccess string.
				UpdateOutput(ifsuccess, MessageType.Info);
			}
			catch (Exception e)
			{
				// If the transmission is not succesful, the exception message
				// will be shown to the user and "can_send" will be set to "Y"
				// to indicate that new data can be sent to the robot.
				message = e.InnerException == null ? e.Message : e.InnerException.Message;
				UpdateOutput("Send() exception: " + message, MessageType.Error);

				can_send = true;
				dataRecv = "Y";
			}
		}
		else
		{
			UpdateOutput("Wait until the end of the current action", MessageType.Warn);
		}
	}

	void ReadCallback (IAsyncResult res) 
	{
		NetworkStream stream;

		try
		{
			UpdateOutput("Reading from socket", MessageType.Info);

			stream = client.GetStream ();

			byte[] recvData = res.AsyncState as byte[];

			stream.EndRead(res);

			var dataRecvString = System.Text.Encoding.UTF8.GetString(recvData, 0, 1);

			dataRecv = System.Text.Encoding.UTF8.GetString(recvData, 0, 1);
			dataRecv = "Y";
			UpdateOutput("Received -" + dataRecv + "-", MessageType.Info);

			// Once new data can be sent the socket is closed.
			if (dataRecv.CompareTo("Y") == 0)
			{
				stream.Close();
				client.Close();
			}
		}
		catch (Exception e)
		{
			// If something in the communication fails, the exception message
			// will be shown to the user and dataRecv will be set to "Y"
			// to indicate that new data can be sent to the robot (only the
			// main thread can modify player preferences).
			message = e.InnerException == null ? e.Message : e.InnerException.Message;
			UpdateOutput("ReadCallback() exception: " + message, MessageType.Warn);
			dataRecv = "Y";
		}
	}

	private ConnectionState state = ConnectionState.CONNECTION_INIT;
	//	private Gson gson = new Gson();

	/**
 * Send {@link NAOCommands} to remote NAO.
 * @param aCommand	{@link NAOCommands} to send.
 * @param aArgs		Array of {@link String} arguments to for {@code aCommand}.
 * @return			{@code true} if successful, {@code false} otherwise.
 */
	public bool sendCommand(NAOCommands aCommand, string[] aArgs)
	{
		if (state == ConnectionState.CONNECTION_ESTABLISHED)
		{
			DataRequestPackage commandObject = new DataRequestPackage(aCommand, aArgs);

			var json = JsonUtility.ToJson(commandObject);

			Send(json, "Success of sendCommand(" + aCommand + " " + aArgs + ")");

			return true;
		}
		return false;
	}
		
	public void UpdateOutput(string msg, MessageType type)
	{
		ConnectPanel.instance.outputText.text += type + " :: " + msg + "\n";

		switch(type)
		{
			case MessageType.Info:	Debug.Log(msg); break;
			case MessageType.Warn:	Debug.LogWarning(msg); break;
			case MessageType.Error:	Debug.LogError(msg); break;
		}
	}
}

public enum MessageType
{
	Info,
	Warn,
	Error
}
