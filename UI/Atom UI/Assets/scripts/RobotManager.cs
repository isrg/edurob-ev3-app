﻿using UnityEngine;
using System;

public class RobotManager : MonoBehaviour
{
	public static RobotManager Instance;
	public EV3 Ev3;
	public NAO Nao;

	public Robot ActiveRobot
	{
		get
		{
			if (Selected == RobotType.EV3)
			{
				return EV3.Instance;
			}
			else if (Selected == RobotType.NAO)
			{
				return Nao;
			}
			return null;
		}
	}

	public static RobotType Selected
	{
		get
		{
			if (PlayerPrefs.HasKey("RobotType"))
				return (RobotType)PlayerPrefs.GetInt("RobotType");
			else
				return RobotType.None;

		}
		set
		{
			PlayerPrefs.SetInt("RobotType", (int)value);
		}
	}

	void Awake()
	{
		if (Instance != null)
			throw new Exception("An instance of " + this.GetType() + " already exists!");

		Instance = this;
	}

	void Start()
	{
		
	}
	
	void Update()
	{
	
	}

	public void SelectRobot(string r)
	{
		Selected = (RobotType)Enum.Parse(typeof(RobotType), r);
	}
}
