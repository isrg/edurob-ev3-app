﻿using UnityEngine;
using System;

[Serializable]
public class NAO : Robot
{
	public new readonly RobotType type = RobotType.NAO;

//	static NAO _instance;
//
//	public static NAO Instance
//	{
//		get
//		{
//			if (_instance == null)
//				_instance = new NAO(); //CreateInstance<NAO>();
//			return _instance;
//		}
//	}

	public static string ipAddress
	{
		get { return PlayerPrefs.GetString("NAOipAddress", "192.168.0.1"); }
		set { PlayerPrefs.SetString("NAOipAddress", value); }
	}

	public static int port
	{
		get { return PlayerPrefs.GetInt("NAOport", 9559); }
		set { PlayerPrefs.SetInt("NAOport", value); }
	}

	public NAO()
	{
		Debug.Log("NAO instance created");
	}

}
