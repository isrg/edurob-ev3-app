﻿using UnityEngine;
using UnityEngine.UI;
using System.Net;

public class UIPanelWifi : MonoBehaviour
{
	public UIScreenBase.ViewText viewText;
	 
	public InputField IPAddressEntry;
	public InputField Port;
	public Button ConnectButton;
	IPAddress address;

	void Start ()
	{
		ConnectButton.interactable = false;
		IPAddressEntry.text = NAO.ipAddress;
		Port.text = NAO.port.ToString();
	}

	void Update ()
	{
		
	}

	public void Connect()
	{
		SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"animationHello"} );
	}

	public void IPValueChanged()
	{
		string ipString = IPAddressEntry.text;

		if (ipString.IsIPv4())
		{
			NAO.ipAddress = ipString;
			ConnectButton.interactable = true;
		}
		else
			ConnectButton.interactable = false;

	}

	public void IPEndEdit()
	{
		if (!IPAddressEntry.text.IsIPv4())
			IPAddressEntry.text = NAO.ipAddress;
	}

	public void PortValueChanged()
	{
		/*
		 * 
		 * You can create a server on ports 1 through 65535.
		 * Port numbers less than 256 are reserved for well-known
		 * services (like HTTP on port 80) and port numbers less
		 * than 1024 require root access on UNIX systems.
		 * Specifying a port of 0 in the ServerSocket constructor
		 * results in the server listening on a random, unused
		 * port, usually >= 1024.
		 * 
		 */

		if  (Port.text == string.Empty)
			return;
		
		var port = int.Parse(Port.text);
		Port.text = port.ToString();

		if (port < 0)
			Port.text = (-port).ToString();
		if (port > 65535)
			Port.text = "65535";
			
		NAO.port = int.Parse(Port.text);
	}

	public void PortValueEndEdit()
	{
		if (Port.text == string.Empty)
			Port.text = "9559";

		NAO.port = int.Parse(Port.text);
	}

//
//	public void onClick(View v) {
//		if( v == btnFunctionHello ){			
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"animationHello"} );
//
//		} else if( v == btnFunctionsCustomNewAdd ){
//
//			string vEventKey = txtFunctionsCustomNewALMemoryKey.getText().toString();
//			string vEventName = txtFunctionsCustomNewName.getText().toString();
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_ADD, new string[]{vEventKey, vEventName} );
//
//		} else if( v == btnFunctionsFaceTracker ){			
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"functionFaceTracker"} );
//
//		} else if( v == btnFunctionShakeHands ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"animationShakeHands"} );
//
//		} else if( v == btnFunctionsRedBallTracker ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"functionRedBallTracker"} );
//
//		} else if( v == btnFunctionStandUp ){
//			SendData.Instance.sendCommand( NAOCommands.STAND_UP );
//
//		} else if( v == btnFunctionWipeForehead ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"animationWipeForehead"} );
//
//		} else if( v == btnFuntctionSitDown ){
//			SendData.Instance.sendCommand( NAOCommands.SIT_DOWN );
//
//		} else if( v == btnDanceCaravanPalace ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"danceCaravanPalace"} );
//
//		} else if( v == btnDanceEvolutionOfDance ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"danceEvolutionOfDance"} );
//
//		} else if( v == btnDanceEyeOfTheTiger ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"danceEyeOfTheTiger"} );
//
//		} else if( v == btnDanceGangnameStyle ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"danceGangnamStyle"} );
//
//		} else if( v == btnDanceThaiChi ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"danceThaiChi"} );
//
//		} else if( v == btnDanceVangelisDance ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"danceVangelisDance"} );
//
//		} else if( v == btnFunctionsAbort ){
//			SendData.Instance.sendCommand( NAOCommands.MEMORY_EVENT_RAISE, new string[]{"naocomAbort"} );
//
//		}
//	}

}
	