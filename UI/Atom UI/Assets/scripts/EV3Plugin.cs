﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class EV3Plugin : MonoBehaviour
{
	#region Properties

	public static EV3Plugin Instance;

	public static AndroidJavaClass UnityPlayer;
	public static AndroidJavaObject Activity;

	AndroidJavaClass activityClass = null;
	AndroidJavaObject activityContext = null;

	AndroidJavaObject ev3Plugin = null;

	public bool DynamicScriptLocation = false;
	public bool InitialiseOnStart = false;
	public bool autoEnableBT = false;

	public bool BluetoothIsConnected { get; private set; }

//	public EV3Configuration EV3Config;
//	public AtomSettings EV3AtomSettings;

	[NonSerialized]
	public string DebugStatus = string.Empty;

	public int invert
	{
		get { return EV3.Instance.EV3Config.InvertMotors ? -1 : 1; }
	}

	enum ConnectionError
	{
		None,
		BluetoothNotAvailable,
		NoPairedDevices,
		DeviceNameNotFound,
		SocketCreationFailed,
		SocketConnectionFailed
	}

	#region Events

	public delegate void PortDeviceUpdateEventHandler(InputPort port, Device device);
	public static event PortDeviceUpdateEventHandler PortDeviceUpdateEvent;

	public delegate void PortDataUpdateEventHandler(PortState state);
	public static event PortDataUpdateEventHandler PortDataUpdateEvent;

	// Sensor events

	public delegate void InfraredSensorUpdateEventHandler(PortState state);
	public static event InfraredSensorUpdateEventHandler InfraredSensorUpdateEvent;

	public delegate void ColourSensorUpdateEventHandler(PortState state);
	public static event ColourSensorUpdateEventHandler ColourSensorUpdateEvent;

	public delegate void TouchSensorUpdateEventHandler(PortState state);
	public static event TouchSensorUpdateEventHandler TouchSensorUpdateEvent;

	public delegate void UltrasonicSensorUpdateEventHandler(PortState state);
	public static event UltrasonicSensorUpdateEventHandler UltrasonicSensorUpdateEvent;

	public delegate void GyroscopeSensorUpdateEventHandler(PortState state);
	public static event GyroscopeSensorUpdateEventHandler GyroscopeSensorUpdateEvent;

	// Bluetooth

	public delegate void BluetoothUpdateEventHandler(bool state);
	public static event BluetoothUpdateEventHandler BluetoothUpdateEvent;

	// Buttons

	public delegate void ButtonUpdateEventHandler(BrickButton button, bool state);
	public static event ButtonUpdateEventHandler ButtonUpdateEvent;

	#endregion Events

	#endregion Properties

	#region Loop

	void Awake()
	{
		Instance = this;
		if (Debug.isDebugBuild)
			DebugStatus = "Development build enabled (v" + Application.version + ")";
		else
			DebugStatus = "Production build (v" + Application.version + ")";
	}

	void Start ()
	{

		// Register events

		PortDeviceUpdateEvent += 
			EV3Manager_throwPortDeviceUpdateEvent;

		PortDataUpdateEvent += 
			EV3Manager_throwPortDataUpdateEvent;

		// Sensor Events

		InfraredSensorUpdateEvent += 
			EV3Manager_InfraredSensorUpdateEvent;

		ColourSensorUpdateEvent += 
			EV3Manager_ColourSensorUpdateEvent;

		TouchSensorUpdateEvent += 
			EV3Manager_TouchSensorUpdateEvent;

		UltrasonicSensorUpdateEvent += 
			EV3Manager_UltrasonicSensorUpdateEvent;

		GyroscopeSensorUpdateEvent += 
			EV3Manager_GyroscopeSensorUpdateEvent;

		BluetoothUpdateEvent += 
			EV3Manager_BluetoothUpdateEvent;

		// Buttons

		ButtonUpdateEvent += 
			EV3Manager_ButtonUpdateEvent;
		
		try
		{
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			activityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
			activityContext = activityClass.GetStatic<AndroidJavaObject>("currentActivity");

			DebugStatus = "Found unity plugin!";

			using (AndroidJavaClass pluginClass = new AndroidJavaClass("uk.co.dyadica.ev3api.EV3Manager"))
			{
				if (pluginClass != null)
				{
					DebugStatus = "Found ev3 plugin!";

					ev3Plugin = pluginClass.CallStatic<AndroidJavaObject>("ev3Manager");
					ev3Plugin.Call("setContext", activityContext);
				}
			}

			if (DynamicScriptLocation)
				ev3Plugin.CallStatic<string>("scriptLocation", gameObject.name);

			if (autoEnableBT)
				ev3Plugin.CallStatic<bool>("autoEnableBT", true);

			if(InitialiseOnStart)
				ev3Plugin.Call("initialisePlugin");
		}
		catch (Exception ex)
		{
			#if UNITY_EDITOR
			Debug.Log(ex.Message + " Init of AndroidJavaClass failed: this is the Editor, so not a problem.");
			#else
			Debug.LogError(ex);
			#endif
		}

	}

	void OnDestroy()
	{
		PortDeviceUpdateEvent -=
			EV3Manager_throwPortDeviceUpdateEvent;

		PortDataUpdateEvent -=
			EV3Manager_throwPortDataUpdateEvent;

		// Sensors

		InfraredSensorUpdateEvent -=
			EV3Manager_InfraredSensorUpdateEvent;

		ColourSensorUpdateEvent -=
			EV3Manager_ColourSensorUpdateEvent;

		TouchSensorUpdateEvent -=
			EV3Manager_TouchSensorUpdateEvent;

		UltrasonicSensorUpdateEvent -=
			EV3Manager_UltrasonicSensorUpdateEvent;

		GyroscopeSensorUpdateEvent -=
			EV3Manager_GyroscopeSensorUpdateEvent;

		BluetoothUpdateEvent -=
			EV3Manager_BluetoothUpdateEvent;

		// Buttons

		ButtonUpdateEvent -=
			EV3Manager_ButtonUpdateEvent;
	}

	#endregion Loop

	#region Events

	private void EV3Manager_BluetoothUpdateEvent(bool state)
	{
		// throw new System.NotImplementedException();
	}

	private void EV3Manager_GyroscopeSensorUpdateEvent(PortState state)
	{
		// throw new System.NotImplementedException();
	}

	private void EV3Manager_UltrasonicSensorUpdateEvent(PortState state)
	{
		// throw new System.NotImplementedException();
	}

	private void EV3Manager_TouchSensorUpdateEvent(PortState state)
	{
		// throw new System.NotImplementedException();
	}

	private void EV3Manager_ColourSensorUpdateEvent(PortState state)
	{
		// throw new System.NotImplementedException();
	}

	private void EV3Manager_InfraredSensorUpdateEvent(PortState state)
	{
		// throw new System.NotImplementedException();
	}

	private void EV3Manager_throwPortDataUpdateEvent(PortState state)
	{
		// throw new System.NotImplementedException();
	}

	private void EV3Manager_throwPortDeviceUpdateEvent(InputPort port, Device device)
	{
		// throw new System.NotImplementedException();
	}

	private void EV3Manager_ButtonUpdateEvent(BrickButton button, bool state)
	{
		// throw new System.NotImplementedException();
	}

	#endregion Events

	#region Command Methods

	public void InitialisePlugin()
	{
		var ev3Name =  EV3.EV3Name;

		if (!EV3Plugin.Instance.BluetoothIsConnected)
		{
			#if !UNITY_EDITOR
			ev3Plugin.SetStatic<string>("deviceName", ev3Name);
			ev3Plugin.Call("initialisePlugin");
			#endif
		}
	}

	public void PlayProgram(string fileName)
	{
		ev3Plugin.Call("playProgram", fileName);
	}

	public void PlayAudio(int volume, string fileName)
	{
		ev3Plugin.Call("playAudio", volume, fileName);
	}

	public void CallLedPattern(LedPattern pattern)
	{
		ev3Plugin.Call("setLedPattern", pattern.ToString());
	}

	public void StopDriveMotors()
	{
		var driveMotorPorts = EV3.GetMotorConfig();

		ev3Plugin.Call("stopDriveMotors", driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString());
	}

	public void StartDriveMotors(string leftPort, string rightPort, int leftSpeed, int rightSpeed)
	{
		ev3Plugin.Call("startDriveMotors", leftPort, rightPort, leftSpeed, rightSpeed);
	}

	public void StartDriveMotors(Direction direction)
	{
		var driveMotorPorts = EV3.GetMotorConfig();

		var speed = 100;
		var v = speed * invert;

		switch(direction)
		{
			case Direction.Forward:
				ev3Plugin.Call("startDriveMotors", driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString(), v, v);
				break;

			case Direction.Backwards:
				ev3Plugin.Call("startDriveMotors", driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString(), -v, -v);
				break;

			case Direction.Left:
				ev3Plugin.Call("startDriveMotors", driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString(), v, -v);
				break;

			case Direction.Right:
				ev3Plugin.Call("startDriveMotors", driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString(), -v, v);
				break;
		}
	}

	#endregion Command Methods


	#region Input Triggers

	void throwAlertUpdate(string data)
	{
		DebugStatus = data;
	}

	void throwBluetoothConnection(string data)
	{
		switch (data)
		{
			case "true": BluetoothIsConnected = true; break;
			case "false": BluetoothIsConnected = false; break;
		}

		if (BluetoothUpdateEvent != null)
			BluetoothUpdateEvent(BluetoothIsConnected);
		
		DebugStatus = "Bluetooth connected: " + data;
	}

	void throwBluetoothAlert(string data)
	{
		DebugStatus = "Bluetooth adapter " + (data == "true" ? "enabled" : "disabled");
		//        string[] input = data.Split(',');
	}

	void throwBluetoothConnectionError(string data)
	{
		DebugStatus = "Bluetooth connection failed: " + data;

		var connectionError = (ConnectionError) Enum.Parse(typeof(ConnectionError), data);

		switch (connectionError)
		{
			case ConnectionError.BluetoothNotAvailable:
				// No bluetooth
				DebugStatus += "\nCannot find Bluetooth.";
				break;
			case ConnectionError.NoPairedDevices:
			case ConnectionError.DeviceNameNotFound:
				// Device not paired
				DebugStatus += "\nCannot find a paired device with the name " + EV3.EV3Name;
				break;
			case ConnectionError.SocketCreationFailed:
			case ConnectionError.SocketConnectionFailed:
				// Device not connected
				DebugStatus += "\nCannot connect to " + EV3.EV3Name + " via Bluetooth. Ensure that it is switched on.";
				break;
		}
	}


	void throwButtonUpdate(string data)
	{
		string[] input = data.Split(',');

		if (input.Length == 2)
		{
			string buttonType = input[0];
			string buttonState = input[1];

			BrickButton bb = (BrickButton)System.Enum.Parse(typeof(BrickButton), buttonType);
			bool state = bool.Parse(buttonState);

			if (ButtonUpdateEvent != null)
				ButtonUpdateEvent(bb, state);

			// print("Button: " + bb.ToString() + "," + state.ToString());
		}
	}

	void throwPortDeviceUpdate(string data)
	{
		string[] input = data.Split(',');

		if(input.Length == 2)
		{
			string deviceType = input[1];
			string devicePort = input[0];

			Device d = (Device)System.Enum.Parse(typeof(Device), deviceType);
			InputPort p = (InputPort)System.Enum.Parse(typeof(InputPort), devicePort);

			if (PortDeviceUpdateEvent != null)
				PortDeviceUpdateEvent(p, d);

//			 print("PortDeviceUpdate: " + p + "," + d);
		}
	}

	void throwPortSensorUpdate(string data)
	{
		string[] input = data.Split(',');

		if (input.Length == 6)
		{
			string devicePort = input[0];
			string deviceType = input[1];
			string deviceMode = input[2];

			string ValueR = input[3];
			string ValueS = input[4];
			string ValueP = input[5];

//			 print("PortSensorUpdate: " + devicePort + "," + deviceType + "," + deviceMode + "," + ValueR);

			PortState portState = new PortState();

			portState.InputPort = (InputPort)System.Enum.Parse(typeof(InputPort), devicePort);
			portState.Device = (Device)System.Enum.Parse(typeof(Device), deviceType);
			portState.Mode = int.Parse(deviceMode);
			portState.Raw = double.Parse(ValueR);
			portState.SI = double.Parse(ValueS);
			portState.Percent = double.Parse(ValueP);

			if (PortDataUpdateEvent != null)
				PortDataUpdateEvent(portState);

			// Throw sensor events

			if(portState.Device == Device.Color)
			{
				if (ColourSensorUpdateEvent != null)
					ColourSensorUpdateEvent(portState);
			}

			if(portState.Device == Device.Infrared)
			{
				if (InfraredSensorUpdateEvent != null)
					InfraredSensorUpdateEvent(portState);
			}

			if(portState.Device == Device.Touch)
			{
				if (TouchSensorUpdateEvent != null)
					TouchSensorUpdateEvent(portState);
			}

			if(portState.Device == Device.Gyroscope)
			{
				if (GyroscopeSensorUpdateEvent != null)
					GyroscopeSensorUpdateEvent(portState);
			}

			if(portState.Device == Device.Ultrasonic)
			{
				if (UltrasonicSensorUpdateEvent != null)
					UltrasonicSensorUpdateEvent(portState);
			}
		}
	}

	#endregion Input Triggers
//
//	public void LoadConfig()
//	{
//		EV3Config = LoadFromPrefs(EV3Name);
//	}
//
//	public void SaveConfig()
//	{
//		SaveToPrefs(EV3Name, EV3Config);
//	}
//
//	public static EV3Configuration LoadFromPrefs(string EV3Name)
//	{
//		var ev3ConfigString = PlayerPrefs.GetString(EV3Name + "_config", "empty");
//		Debug.Log("Load " + EV3Name + "_config : " + ev3ConfigString);
//		if (ev3ConfigString == "empty")
//		{
//			var EV3Config = new EV3Configuration(EV3Name);
//			SaveToPrefs(EV3Name, EV3Config);
//			return EV3Config;
//		}
//		var scenarioSettings = Extensions.Deserialize<EV3Configuration>(ev3ConfigString);
//		return scenarioSettings;
//	}
//
//	public static void SaveToPrefs(string EV3Name, EV3Configuration ev3ConfigString)
//	{
//		var save = Extensions.Serialize<EV3Configuration>(ev3ConfigString);
//		Debug.Log("Save " + EV3Name + "_config : " + save);
//		PlayerPrefs.SetString(EV3Name + "_config", save);
//	}
}

//[Serializable]
//public class EV3Configuration
//{
//	public string EV3Name;
//
//	public List<string> ProgramNames;
//
////	[NonSerialized]	public MotorPort SMotorPort;
//	[NonSerialized]	public MotorPort LMotorPort;
//	[NonSerialized]	public MotorPort RMotorPort;
//
//	[NonSerialized]	public MotorPort PortA = new MotorPort(OutputPort.A);	
//	[NonSerialized] public MotorPort PortB = new MotorPort(OutputPort.B);
//	[NonSerialized] public MotorPort PortC = new MotorPort(OutputPort.C);
//	[NonSerialized] public MotorPort PortD = new MotorPort(OutputPort.D);
//
//	[NonSerialized]	public SensorPort Port1 = new SensorPort(InputPort.One);	
//	[NonSerialized] public SensorPort Port2 = new SensorPort(InputPort.Two);
//	[NonSerialized] public SensorPort Port3 = new SensorPort(InputPort.Three);
//	[NonSerialized] public SensorPort Port4 = new SensorPort(InputPort.Four);
//
//	[NonSerialized] public List<MotorPort> MotorPorts;
//	[NonSerialized] public List<SensorPort> SensorPorts;
//
//	public bool InvertMotors;
//
//	public EV3Configuration() {}
//
//	public EV3Configuration(string name)
//	{
//		EV3Name = name;
//
//		ProgramNames = new List<string>();
//
//		PortA.MotorType = MotorType.Small;
//		PortB.MotorType = MotorType.Large;
//		PortC.MotorType = MotorType.Large;
//		PortD.MotorType = MotorType.None;
//
//		Port1.SensorType = SensorType.Touch;
//		Port2.SensorType = SensorType.Gyro;
//		Port3.SensorType = SensorType.Colour;
//		Port4.SensorType = SensorType.Ultrasonic;
//
////		SMotorPort = PortA;
//		LMotorPort = new MotorPort();
//		RMotorPort = new MotorPort();
//
//		MotorPorts = new List<MotorPort> {PortA, PortB, PortC, PortD};
//		SensorPorts = new List<SensorPort> {Port1, Port2, Port3, Port4};
//	}
//
//	public class MotorPort
//	{
//		public OutputPort Port;
//		public MotorType MotorType = MotorType.None;
//
//		public MotorPort() {}
//
//		public MotorPort(OutputPort port)
//		{
//			Port = port;
//		}
//	}
//
//	public class SensorPort
//	{
//		public InputPort Port;
//		public SensorType SensorType;
//
//		public SensorPort() {}
//
//		public SensorPort(InputPort port)
//		{
//			Port = port;
//		}
//	}
//}
