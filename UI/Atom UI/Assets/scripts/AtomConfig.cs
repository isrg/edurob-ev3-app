﻿using UnityEngine;
using System.Collections.Generic;

[CreateAssetMenu]
public class AtomConfig : ScriptableObject
{
	public string AtomID;
	public string Label;
	public Sprite Sprite;
	public float Scale = 0.7f;
	public GameObject Prefab;
	public State StageState;
	public List<AtomConfig> Options;

	public enum State
	{
		Intermediate,
		Final,
		EndScenario
	}
}
