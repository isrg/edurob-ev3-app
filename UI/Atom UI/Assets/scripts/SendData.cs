// SendData.cs: Sends data to the server running on the robot.

// Author: Maria Jose Galvez Trigo
//
// Copyright (C) 2014 Maria Jose Galvez Trigo GNU GPLv3

using UnityEngine;
using System;
using System.Linq;
using System.Text;
using System.IO;
using System.Collections;
using System.Net;
using System.Net.Sockets;

public class SendData : ScriptableObject 
{
	// Variables needed to create the client and send data.
	TcpClient client;
	Stream stream;

	string message;
	string dataToSend;
	string dataRecv;

	byte[] messageData;
	byte[] recvData;

	// Pop-up message that will show feedback to the user.
	GameObject popUpMessage;

	// Static variable to the SendData instance.
	private static SendData _instance;

	public static SendData Instance 
	{ 
		get
		{
//			if(_instance == null)
//			{
//				_instance = GameObject.FindObjectOfType<SendData>();
//
//				// Tell unity not to destroy this object when loading a 
//				// new scene
//				DontDestroyOnLoad(_instance.gameObject);
//			}
//
//			return _instance;

			if (_instance == null)
			{
				_instance = ScriptableObject.CreateInstance<SendData>();
				DontDestroyOnLoad(_instance);
			}
			return _instance;
		}
	}

	// When the object awakens, we assign the instance variable.
	void Awake ()
	{
		if(_instance == null)
		{
			//If I am the first instance, make me the Singleton
			_instance = this;
			DontDestroyOnLoad(this);
		}
		else
		{
			//If a Singleton already exists and you find
			//another reference in scene, destroy it!
			if(this != _instance)
				Destroy(this); //Destroy(this.gameObject);
		}
	}

	void Start()
	{
		
	}

	void Update () 
	{
		if (dataRecv != null) 
		{
			PlayerPrefs.SetString("can_send", dataRecv);
		}
	}

	string alertTitle = "Communication Alert";

	// Parametre data is the data that will be sent to the robot.
	// Parametre ifsuccess defines the message that will be shown to the user
	// if the data is successfully sent.
	public void Send (string data, string ifsuccess) 
	{    
		try
		{
			client = new TcpClient(AddressFamily.InterNetwork);
			client.Connect(NAO.ipAddress, NAO.port);
		}
		catch (Exception e)
		{
			if (e.InnerException == null)
				message = e.Message;
			else
				message = e.InnerException.Message;

			UIMessage.Alert(alertTitle, message);
			PlayerPrefs.SetString("can_send", "Y");
			Debug.Log(message);
			Debug.Log(e.StackTrace);
		}
		return;

//		Debug.Log("Value of can_send: -" + PlayerPrefs.GetString("can_send") + "-");
//
//		// If the last answer from the server in the robot has been "N",
//		// indicating that the robot is busy, the client won't send any data.
//		if (!PlayerPrefs.HasKey("can_send") ||
//			(PlayerPrefs.GetString("can_send").CompareTo("Y") == 0))
//		{
//			dataRecv = "N";
//			PlayerPrefs.SetString("can_send", dataRecv);
//
//			try 
//			{
//				dataToSend = data;
//
//				// A client for the server IP and the port 9558 is established.
//				client = new TcpClient(AddressFamily.InterNetwork);
//				client.Connect(NAO.ipAddress, NAO.port);
//
//				// The data is encoded.
//				messageData = Encoding.UTF8.GetBytes(dataToSend);
//
//				recvData = new byte[client.ReceiveBufferSize];
//
//				// And written to the socket.
//				NetworkStream stream = client.GetStream();
//				stream.Write(messageData, 0, messageData.Length);
//
//				Debug.Log("Data written");
//
//				// If everything goes well we will receive two answers, one
//				// containing "N" indicating that the robot is already
//				// performing an activity and can't receive new data, and
//				// a second one containing "Y" indicating that new data can
//				// be sent to the robot.
//				stream.BeginRead (recvData, 0, 1, ReadCallback, recvData);
//
//				stream.BeginRead (recvData, 0, 1, ReadCallback, recvData);
//
//				Debug.Log("Final data" + dataRecv);
//
//				// If the data has been successfully sent the pop-up message 
//				// will be the text in the ifsuccess string.
//				message = ifsuccess;
//
//				UIMessage.Alert(alertTitle, ifsuccess);
//			}
//			catch (Exception e)
//			{
//				// If the transmission is not succesful, the exception message
//				// will be shown to the user and "can_send" will be set to "Y"
//				// to indicate that new data can be sent to the robot.
//				if (e.InnerException == null)
//					message = e.Message;
//				else
//					message = e.InnerException.Message;
//
//				UIMessage.Alert(alertTitle, message);
//				PlayerPrefs.SetString("can_send", "Y");
//				Debug.Log(message);
//			}
//		}
//		else
//		{
//			message = "Wait until the end of the current action";
//			UIMessage.Alert(alertTitle, message);
//		}
	}

	void ReadCallback (IAsyncResult res) 
	{
		NetworkStream stream;

		try
		{
			Debug.Log("Reading from socket");

			stream = client.GetStream ();

			byte[] recvData = res.AsyncState as byte[];

			stream.EndRead(res);

			dataRecv = System.Text.Encoding.UTF8.GetString(
				recvData, 0, 1);

			Debug.Log("Received -" + dataRecv + "-");

			// Once new data can be sent the socket is closed.
			if (dataRecv.CompareTo("Y") == 0)
			{
				stream.Close();
				client.Close();
			}
		}
		catch (Exception e)
		{
			// If something in the communication fails, the exception message
			// will be shown to the user and dataRecv will be set to "Y"
			// to indicate that new data can be sent to the robot (only the
			// main thread can modify player preferences).
			Debug.Log(e.InnerException.Message);
			message = e.InnerException.Message;
			dataRecv = "Y";
		}
	}

	void OnApplicationQuit () 
	{
		PlayerPrefs.SetString("can_send", "Y");
	}

	void OnDestroy() 
	{
		PlayerPrefs.SetString("can_send", "Y");
	}



/*
 * 
 * 
 * 
 * 
 *
 *
 */


	private ConnectionState state = ConnectionState.CONNECTION_INIT;
	//	private Gson gson = new Gson();

	/**
 * Send {@link NAOCommands} to remote NAO.
 * @param aCommand	{@link NAOCommands} to send.
 * @param aArgs		Array of {@link String} arguments to for {@code aCommand}.
 * @return			{@code true} if successful, {@code false} otherwise.
 */
	public bool sendCommand(NAOCommands aCommand, String[] aArgs)
	{
		if (state == ConnectionState.CONNECTION_ESTABLISHED)
		{
			DataRequestPackage commandObject = new DataRequestPackage(aCommand, aArgs);

			var json = JsonUtility.ToJson(commandObject);

			Send(json, "WooHoo");

			return true;
		}

		return false;
	}
}