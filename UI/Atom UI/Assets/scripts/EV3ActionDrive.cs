﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class EV3ActionDrive : AtomDisplayBase
{
	public Button ForwardButton;
	public Button LeftButton;
	public Button RightButton;
	public Button ReverseButton;

	public GameObject SpeedSlider;
	public Text SpeedValue;

	public Toggle ToggleSpeedSlider;
	public Toggle ToggleInvertMotors;
	public Toggle ToggleSwapMotors;

	public GameObject LeftMotorInfo;
	public GameObject RightMotorInfo;

	Slider slider;

	InputPort[] driveMotorPorts;

	public override void OnDisable()
	{
		base.OnDisable();

		EV3.PortChangeEvent -= RefreshPortAssignmentDisplay;
	}

	void OnEnableCommon()
	{
		EV3.PortChangeEvent += RefreshPortAssignmentDisplay;

		slider = SpeedSlider.GetComponentInChildren<Slider>();
		slider.value = Settings.EV3Drive.MotorSpeed;
		RefreshSpeedValueDisplay(Settings.EV3Drive.MotorSpeed);

		slider.onValueChanged.AddListener(delegate {
			RefreshSpeedValueDisplay(slider.value);
		});
			
		driveMotorPorts = EV3.GetMotorConfig();
	}

	public override void OnEnableRun(AtomSettings atomSettings)
	{
		base.OnEnableRun(atomSettings);

		OnEnableCommon();

		SpeedSlider.SetActive(Settings.EV3Drive.SliderVisible);
	}

	public override void OnEnableEdit(AtomSettings atomSettings)
	{
		base.OnEnableEdit(atomSettings);

		OnEnableCommon();

		RefreshPortAssignmentDisplay();

		ToggleSpeedSlider.isOn = Settings.EV3Drive.SliderVisible;
		ToggleSpeedSlider.onValueChanged.AddListener(delegate {
			Settings.EV3Drive.SliderVisible = ToggleSpeedSlider.isOn;
		});
		ToggleInvertMotors.isOn = EV3Config.InvertMotors;
		ToggleInvertMotors.onValueChanged.AddListener(delegate {
			EV3Config.InvertMotors = ToggleInvertMotors.isOn;
		});
		ToggleSwapMotors.isOn = EV3Config.SwapMotors;
		ToggleSwapMotors.onValueChanged.AddListener(delegate {
			EV3Config.SwapMotors = ToggleSwapMotors.isOn;
			driveMotorPorts = EV3.GetMotorConfig();
			RefreshPortAssignmentDisplay();
		});
	}

	public void Drive(Direction direction)
	{
		var speed = Settings.EV3Drive.MotorSpeed;
		var invert = EV3Config.InvertMotors ? -1 : 1;
		var v = speed * invert;

		switch(direction)
		{
			case Direction.Forward:
				EV3Plugin.Instance.StartDriveMotors(driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString(), speed, speed);
				break;

			case Direction.Backwards:
				EV3Plugin.Instance.StartDriveMotors(driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString(), -speed, -speed);
				break;

			case Direction.Left:
				EV3Plugin.Instance.StartDriveMotors(driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString(), v, -v);
				break;

			case Direction.Right:
				EV3Plugin.Instance.StartDriveMotors(driveMotorPorts[0].ToString(), driveMotorPorts[1].ToString(), -v, v);
				break;
		}
	}

	void RefreshPortAssignmentDisplay()
	{
		var l = LeftMotorInfo.GetComponentsInChildren<Text>();
		l[1].text = Device.LMotor.ToString();
		l[2].text = driveMotorPorts[0].ToString();

		var r = RightMotorInfo.GetComponentsInChildren<Text>();
		r[1].text = Device.LMotor.ToString();
		r[2].text = driveMotorPorts[1].ToString();
	}

	void RefreshSpeedValueDisplay(float value)
	{
		var intValue =  Mathf.RoundToInt(value);
		Settings.EV3Drive.MotorSpeed = intValue;
		SpeedValue.text = Mathf.RoundToInt(intValue).ToString();
	}
}
