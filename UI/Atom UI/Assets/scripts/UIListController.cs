﻿using UnityEngine;
using System.Collections;

public class UIListController : UIList<UIListButton>
{
	public int numCells;

	#region implemented abstract members of UIList

	public override int NumberOfCells()
	{
		return numCells;
	}

	public override void UpdateCell(int index, UIListButton cell)
	{
		cell.cellLabel.text = "Scenario " + index;
	}

	#endregion

	void OnEnable()
	{
		OnCreateList();
	}

	public void OnCreateList()
	{
		int cells = NumberOfCells();

		if (cells > 0)
		{
			ClearAllCells();
			Refresh();
		}
	}
}
