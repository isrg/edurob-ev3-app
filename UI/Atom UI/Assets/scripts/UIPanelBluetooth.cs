﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIPanelBluetooth : MonoBehaviour
{
	public InputField EV3NameEntry;
	public Toggle IndicatorToggle;
	public UIScreenBase.ViewText viewText;

	void OnEnable()
	{
		EV3NameEntry.text = EV3.EV3Name;
		IndicatorToggle.isOn = EV3Plugin.Instance.BluetoothIsConnected;
	}
	
	void Update()
	{
		if (EV3Plugin.Instance.BluetoothIsConnected != IndicatorToggle.isOn)
			UpdateConnectionButtonState();
	}

	void OnDisable()
	{
		UpdateEV3Name();			
	}

	void UpdateConnectionButtonState()
	{
		IndicatorToggle.isOn = EV3Plugin.Instance.BluetoothIsConnected;

		if (IndicatorToggle.isOn)
			UIManager.Instance.SwitchViews(UIManager.Instance.ViewMain);
	}

	public void UpdateEV3Name()
	{
		EV3.EV3Name = EV3NameEntry.text;
	}

	public void Connect()
	{
		UpdateEV3Name();			
		EV3Plugin.Instance.InitialisePlugin();
	}
}
