﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class EV3ActionDriveButton : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	public Direction Direction;

	public void OnPointerDown(PointerEventData eventData)
	{
		GetComponentInParent<EV3ActionDrive>().Drive(Direction);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		EV3Plugin.Instance.StopDriveMotors();
	}
}
