﻿using System;
using UnityEngine;

public class AtomDisplayBase : MonoBehaviour
{
	[NonSerialized] public EV3Configuration EV3Config;

	public UIViewEdit.ViewText ViewTextEdit;
	public UIViewEdit.ViewText ViewTextRun;

	public GameObject EditorOnly;

	public AtomSettings Settings;

	int enableCount = 0;

	protected virtual void Awake()
	{
		EV3Config = EV3.Instance.EV3Config;
	}

	protected virtual void OnEnable()
	{
		enableCount += 1;
		if (enableCount > 1)
			Debug.LogWarning("Unexpected re-enabling of Atom!");
	}

	protected virtual void Start()
	{

	}
		
	public virtual void OnEnableEdit(AtomSettings atomSettings)
	{
		Settings = atomSettings;
		EditorOnly.SetActive(true);
	}

	public virtual void OnEnableRun(AtomSettings atomSettings)
	{
		Settings = atomSettings;
		EditorOnly.SetActive(false);
	}

	protected virtual void Update()
	{

	}

	public virtual void OnDisable()
	{

	}
}
