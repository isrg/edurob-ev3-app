﻿using UnityEngine;
using System;

public class Robot
{
	public readonly RobotType type = RobotType.None;

	public Sprite picture;
	public static Sprite sprite;

	public bool Wifi;
	public bool BlueTooth;
}

public enum RobotType
{
	None,
	NAO,
	EV3
};
