﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class EV3ActionRunProgram : AtomDisplayBase
{
	public Dropdown SelectProgram;
	public Text ProgramTitle;
	public Text ProgramDescription;
	public InputField InputProgramDescription;

	const string ev3DemoProgramName = "BrkProg_SAVE/Demo";

	void OnEnableCommon()
	{
		ProgramTitle.enabled = !ContentManager.EditingActive;
	}

	public override void OnEnableRun(AtomSettings atomSettings)
	{
		base.OnEnableRun(atomSettings);

		OnEnableCommon();

		var screenBase = GetComponentInParent<UIScreenBase>();
		screenBase.viewText.MessageTitle = ViewTextRun.MessageTitle;
		screenBase.SetTitle();

		ProgramTitle.text = UIManager.GetText("running_program") + " " + Settings.EV3Program.ProgramName;
		ProgramDescription.text = Settings.EV3Program.ProgramDescription;
		string progLocation = Settings.EV3Program.ProgramName;
		string progExtension = progLocation.StartsWith("BrkProg_SAVE") ? "rpf" : "rbf";
		string path = "../prjs/" + progLocation + "." + progExtension;
		EV3Plugin.Instance.PlayProgram(path);
	}

	public override void OnEnableEdit(AtomSettings atomSettings)
	{
		base.OnEnableEdit(atomSettings);

		OnEnableCommon();

		SetProgramList();

		SelectProgram.onValueChanged.AddListener(delegate {
			Settings.EV3Program.ProgramName = SelectProgram.captionText.text;
			SetProgramList();
		});

		InputProgramDescription.onValueChanged.AddListener(delegate {
			Settings.EV3Program.ProgramDescription = InputProgramDescription.text;
		});
	}

	void SetProgramList()
	{
		SelectProgram.ClearOptions();

		var demoProg = new List<string>{ev3DemoProgramName};

		if (EV3Config.ProgramPaths.Contains(Settings.EV3Program.ProgramName) || Settings.EV3Program.ProgramName == ev3DemoProgramName)
		{
			SelectProgram.AddOptions(demoProg);
			SelectProgram.AddOptions(EV3Config.ProgramPaths);
			SelectProgram.value = SelectProgram.options.FindIndex(o => o.text == Settings.EV3Program.ProgramName);
		}
		else
		{
			var noProgSelected = new List<string>{UIManager.GetText("no_prog_selected")};

			SelectProgram.AddOptions(noProgSelected);
			SelectProgram.AddOptions(demoProg);
			SelectProgram.AddOptions(EV3Config.ProgramPaths);

			Settings.EV3Program.ProgramDescription = string.Empty;
		}

		if (Settings.EV3Program.ProgramName == ev3DemoProgramName)
			InputProgramDescription.text = UIManager.GetText("ev3_demo_description");
		else
			InputProgramDescription.text = Settings.EV3Program.ProgramDescription;
	}
}
