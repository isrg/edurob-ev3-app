﻿using UnityEngine;
using UnityEngine.UI;

public class Atom : MonoBehaviour
{
	public Button AtomButton;
	public float padding = 30f;
	public Image AtomImage;
	public Text AtomText;

	public GameObject Datum { get; set; }
	public float VerticalOffset { get; set; }
	public AtomConfig Config { get; set; }
	public AtomSettings Settings { get; set; }
	public bool Fixed { get; set; }

	float horizontalTranslation;
	float verticalTranslation = 0f;

	void Awake()
	{
		
	}

	void Start()
	{
		this.name = Config.Label;

		Reposition();

		AtomText.text = Config.Label;
		AtomImage.sprite = Config.Sprite;
		AtomImage.GetComponent<RectTransform>().localScale = Vector3.one * Config.Scale;
	}

	public void Reposition()
	{
		var rectTransform = AtomButton.GetComponent<RectTransform>();

		var scaleFactor = UIManager.Instance.gameObject.GetComponent<Canvas>().scaleFactor;
		horizontalTranslation = (rectTransform.rect.width + padding) * scaleFactor;
		verticalTranslation = (rectTransform.rect.height + padding) * VerticalOffset * scaleFactor;

		transform.localScale = Vector3.one * 0.8f;
		transform.position = Datum.transform.position;
		transform.Translate(new Vector3(horizontalTranslation, verticalTranslation, 0));
	}
}
