﻿using UnityEngine;
using System.Collections;
using System;

public class UIMessageCustom : MonoBehaviour
{
	public string MessageTitle;
	public string MessageContent;

	public void ShowMessage()
	{
		UIMessage.Alert(MessageTitle, MessageContent);
	}
}
