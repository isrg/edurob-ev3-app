﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class UIViewEV3Setup : UIScreenBase
{
	public InputField ProgramList;

	[Serializable]
	public class DeviceItem
	{
		public Sprite Sprite;
		public Device Device;
	}
		
	[Serializable]
	public class MotorPortStateDisplay
	{
		public Image PortImage;
		public Device Device;
	}

	[Serializable]
	public class SensorPortStateDisplay
	{
		public Image PortImage;
		public Device Device;
	}

	public List<MotorPortStateDisplay> MotorPortStateDisplays;
	public List<SensorPortStateDisplay> SensorPortStateDisplays;
	public List<DeviceItem> DeviceItems;

	public Button devAddProgramNames;

	protected override void Awake()
	{
		
	}

	protected override void OnEnable()
	{
		base.OnEnable();

		EV3.Instance.LoadConfig();

		var config = EV3.Instance.EV3Config;

		if (config.ProgramPaths.Count == 0 || (config.ProgramPaths.Count == 1 && config.ProgramPaths[0] == string.Empty))
			ProgramList.text = string.Empty;
		else
			ProgramList.text = String.Join("\n", config.ProgramPaths.ToArray());
			
		ProgramList.onValueChanged.AddListener(delegate {
			var list = ProgramList.text.Split('\n', (char)StringSplitOptions.RemoveEmptyEntries);
			config.ProgramPaths = new List<string>(list);
		});

		RefreshPortDisplay();

		EV3.PortChangeEvent += RefreshPortDisplay;

		devAddProgramNames.enabled = Tokens.isDebugBuild;
	}

	protected override void OnDisable()
	{
		base.OnDisable();

		EV3.Instance.SaveConfig();

		ProgramList.onValueChanged.RemoveAllListeners();

		EV3.PortChangeEvent -= RefreshPortDisplay;
	}

	void RefreshPortDisplay()
	{
		var stateMessage = string.Empty;

		for (int i = 0; i < MotorPortStateDisplays.Count; i++)
		{
			var portItem = MotorPortStateDisplays[i];
			portItem.Device = EV3State.MotorPorts[i];
			var sp = DeviceItems.Find(d => d.Device == portItem.Device);
			portItem.PortImage.sprite = sp.Sprite;

			stateMessage += "Motor " + i + ": " + EV3State.MotorPorts[i] + " - ";
		}

		stateMessage += "\n";

		for (int i = 0; i < SensorPortStateDisplays.Count; i++)
		{
			var portItem = SensorPortStateDisplays[i];
			portItem.Device = EV3State.SensorPorts[i];
			var sp = DeviceItems.Find(d => d.Device == portItem.Device);
			portItem.PortImage.sprite = sp.Sprite;

			stateMessage += "Sensor " + i + ": " + EV3State.SensorPorts[i] + " - ";
		}

		Debug.Log(stateMessage);
	}

	public void dev_AddProgramNames()
	{
		var programPaths = new List<string>
		{
			"Tank Bot/001",
			"Test/MissingProg",
			"BrickColours/BrickColours",
			//	"BrickColours/Program",
			"CloserToMe1/Approximate",
			//	"CloserToMe1/leftbuttonchecksrange",
			"CloserToMe1/speak remote",
			//	"CloserToMe1/speakdistance",
			"CloserToMeUS/Approximate",
			//	"CloserToMeUS/leftbuttonchecksrange",
			"CloserToMeUS/speak remote",
			//	"CloserToMeUS/speakdistance",
			"Follow/Gangnam",
			//	"Follow/KRAZ3_01_BASIC RC",
			//	"Follow/KRAZ3_02_FULL RC",
			//	"Follow/KRAZ3_04_FOLLOW",
			"Follow/show me a colour",
			"GangnamOnly/Gangnam",
			//	"RemoteControl/KRAZ3_02_BASIC RC",
			"RemoteControl/KRAZ3_02_FULL RC",
			"robo_shapes/draw_triangle",
			"robo_shapes/draw_square",
			"robo_shapes/draw_circle",
			"robo_shapes/feedback_pos",
			"robo_shapes/feedback_neg",
			"robo_shapes/say_triangle",
			"robo_shapes/say_square",
			"robo_shapes/say_circle",
			"robo_shapes/reward",
			"Shapes1/Shapes",
			"ShapesGyro/Shapes",
			"ShapesGyro/Triangle",
			"ShapesGyro/Square",
			"ShapesGyro/Circle",
			"ShapesGyro/welldone",
			"ShapesGyro/tryagain",
			"Show Me Colours Crazee3/Show Me Colours"
		};

		ProgramList.text += String.Join("\n", programPaths.ToArray());
	}
}
