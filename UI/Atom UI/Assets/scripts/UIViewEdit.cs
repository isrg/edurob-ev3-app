﻿using UnityEngine;
using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq.Expressions;
using UnityEngine.UI;

public class UIViewEdit : UIScreenBase
{
	public ViewText NameNeeded;

	public InputField ScenarioTitleNameEntry;

	public RectTransform Content;
	public GameObject StartDatum;
	public Atom AtomPrefab;
	public GameObject ConfigPanel;

	public AtomConfig BaseAtom;
	public AtomConfig EndScenario;

	[NonSerialized] public bool IsNewScenario = true;

	List<List<Atom>> scenarioStages;
	int stageIndex;

	protected override void Awake()
	{
		base.Awake();
	}

	protected override void Start()
	{
		base.Start();
	}

	public void UpdateTitle()
	{
		ContentManager.ActiveScenario.ScenarioName = ScenarioTitleNameEntry.text;
		Debug.Log("Saving : " + ScenarioTitleNameEntry.text);
		ContentManager.UpdateScenarioReference();
	}
	
	protected override void OnEnable()
	{
		ContentManager.EditingActive = true;

		var title = ContentManager.ActiveScenario.ScenarioName;
		ScenarioTitleNameEntry.text = title;
		Debug.Log("Editing " + (title == string.Empty ? "New Scenario" : title));

		base.OnEnable();

		UIManager.Instance.common_screen_title.gameObject.SetActive(false);
		ConfigPanel.SetActive(false);

		if (IsNewScenario)
		{
			NewScenario();
			IsNewScenario = false;
		}
		else
		{
			LayoutActiveContent();
		}
	}

	protected override void OnDisable()
	{
		SaveActiveContent();

		UIManager.Instance.common_screen_title.gameObject.SetActive(true);
		ResetEditor();
	}

	protected override void Update()
	{
		base.Update();
	}

	void ResetEditor()
	{
		foreach (Atom staleAtom in Content.GetComponentsInChildren<Atom>())
			Destroy(staleAtom.gameObject);
	}

	void NewScenario()
	{
		var activeAtoms = new List<Atom>();
		Atom atom = NewAtom(BaseAtom, StartDatum, 0f);
		activeAtoms.Add(atom);

		stageIndex = 0;
		scenarioStages = new List<List<Atom>>();
		scenarioStages.Add(activeAtoms);
	}
		
	Atom NewAtom(AtomConfig atomConfig, GameObject datum, float verticalOffset)
	{ 
		Atom atom = Instantiate(AtomPrefab);
		atom.Datum = datum;
		atom.VerticalOffset = verticalOffset;
		atom.Config = atomConfig;
		atom.Settings = new AtomSettings(atomConfig);
		atom.transform.SetParent(Content.transform);
		atom.AtomButton.onClick.AddListener(() => AtomClicked(atom));

		//		a.AtomButton.onClick.AddListener(UIViewEdit.AtomClicked);
		//		a.AtomButton.onClick.AddListener(delegate { Clicked(msg); });
		//		a.AtomButton.onClick.(() => Clicked(msg));

		return atom;
	}

	Atom activeAtom;

	public void AtomClicked(Atom atom)
	{
		activeAtom = atom;

		int selectedStageIndex = scenarioStages.FindIndex(activeAtoms => activeAtoms.Contains(atom));
		var thisStage = scenarioStages[selectedStageIndex];

		Debug.Log("AtomClicked: " + atom.AtomText.text + " (" + atom.Config.StageState + " - Stage: " + selectedStageIndex + ")");

		switch (atom.Config.StageState)
		{
			case AtomConfig.State.Intermediate:
				UpdateDisplayState(atom.Config.Options, atom.Datum);
				break;

			case AtomConfig.State.Final:
				if (thisStage.Count > 1)
				{
					UpdateDisplayState(atom.Config.Options, atom.Datum);
					NewStage();
				}
				else
					StartConfig(atom);
				
				break;

			case AtomConfig.State.EndScenario:
				if (thisStage.Count > 1)
					UpdateDisplayState(new List<AtomConfig> {EndScenario}, atom.Datum);
				else
					UpdateDisplayState(EndScenario.Options, atom.Datum);
				break;
		}
		activeAtom = null;
	}

	void NewStage()
	{
		Atom definedAtom = scenarioStages[stageIndex][0];

		stageIndex++;
		scenarioStages.Add(new List<Atom>());

		UpdateDisplayState(EndScenario.Options, definedAtom.gameObject);
	}

	void UpdateDisplayState(List<AtomConfig> options, GameObject datum)
	{
		var thisStage = scenarioStages[stageIndex];

		for (int i = thisStage.Count - 1; i >= 0 ; i--)
		{
			var staleAtom = thisStage[i];

			if (options.Count > 0 || staleAtom != activeAtom)
			{
				thisStage.RemoveAt(i);
				Destroy(staleAtom.gameObject);
			}
		}

		if (options.Count > 0)
		{
			for (int i = 0; i < options.Count; i++)
			{
				float verticalOffset = (options.Count - 1) / 2f - i;
				Atom newAtom = NewAtom(options[i], datum, verticalOffset);
				thisStage.Add(newAtom);
			}
		}
		else
		{
			activeAtom.VerticalOffset = 0f;
			activeAtom.Reposition();
		}
	}

	GameObject ConfigClone;

	void StartConfig(Atom atom)
	{
		ConfigPanel.SetActive(true);

		activeAtom = atom;
		Debug.Log("SetParams on " + activeAtom.name);

		var target = ConfigPanel.transform.GetChild(0);

		var atomID = activeAtom.Config.AtomID;
		var atomDefinition = ContentManager.AtomLookup[atomID];
		ConfigClone = Instantiate(atomDefinition.Prefab);
		ConfigClone.transform.SetParent(target.transform, false);

		var atomDisplay = ConfigClone.GetComponent<AtomDisplayBase>();
		atomDisplay.OnEnableEdit(activeAtom.Settings);

		var texts = ConfigPanel.GetComponentsInChildren<Text>();
		foreach (var txt in texts)
		{
			if (txt.name.StartsWith("common_"))
				txt.enabled = false;
		}
	}

	public void ConfigComplete()
	{
		Destroy(ConfigClone);
		ConfigPanel.SetActive(false);
		activeAtom = null;
	}

	public void BackButton()
	{
		Debug.LogWarning("Perform checks - Unique name, mark if not runnable");

		if (ScenarioTitleNameEntry.text == string.Empty)
			UIMessage.Alert(NameNeeded.MessageTitle, NameNeeded.MessageContent);
		else
			UIManager.Instance.SwitchViews(UIManager.Instance.ViewMain);
	}

	public void DeleteButton()
	{
		Debug.LogWarning("Ask for confirmation.");

		UIManager.Instance.SwitchViews(UIManager.Instance.ViewMain);
	}

	void SaveActiveContent()
	{
		if (ContentManager.ActiveScenario == null)
			return;

		if (ContentManager.ActiveScenario.ScenarioName == string.Empty)
		{
			ContentManager.ActiveScenario.ScenarioName = "noname " + ContentManager.ActiveScenario.ScenarioID.Substring(0, 8);
			ContentManager.UpdateScenarioReference();
		}

		var scenarioSettings = new List<AtomSettings>();

		for (int i = 0; i < scenarioStages.Count; i++)
		{
			var settings = scenarioStages[i][0].Settings;
			if (i == scenarioStages.Count-1)
				settings = new AtomSettings(EndScenario);

			scenarioSettings.Add(settings);
		}

		ContentManager.ActiveScenario.AtomSettingsList = scenarioSettings;
		ContentManager.SaveScenario();
	}

	void LayoutActiveContent()
	{
		var datum = StartDatum;
		var scenarioSettings = ContentManager.ActiveScenario.AtomSettingsList;

		scenarioStages = new List<List<Atom>>();

		for (int i = 0; i < scenarioSettings.Count; i++)
		{
			var atomSettings = scenarioSettings[i];
			var atomID = atomSettings.ID;

			AtomConfig atomDefinition = ContentManager.AtomLookup[atomID];

			Atom atom = NewAtom(atomDefinition, datum, 0);
			atom.Settings = atomSettings;
			scenarioStages.Add(new List<Atom> {
				atom
			});
			datum = atom.gameObject;
		}
		stageIndex = scenarioSettings.Count - 1;
	}
}

