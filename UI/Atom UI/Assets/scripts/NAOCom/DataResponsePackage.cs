﻿using System;
using System.Collections.Generic;

public class DataResponsePackage
{

	public DataRequestPackage request;
	public bool requestSuccessfull;

	public DataResponsePackage() {}

	public DataResponsePackage(DataRequestPackage aRequest, bool aSuccessful)
	{
		request = aRequest;
		requestSuccessfull = aSuccessful;
	}

	/**
	 * Requested data
	 */
	public long revision;
	public String naoName;
	public int batteryLevel;
	public NAOAutonomousLifeStates lifeState;
	public StiffnessData stiffnessData;
	public AudioData audioData;
	public Dictionary<String, String> customMemoryEvents;

	public String toString(){
		String ret = "";

		ret += "REQUEST: " + request;
		ret += "\nrevision: " + revision;
		ret += "\nnaoName: " + naoName + "\tbatterylevel: " + batteryLevel;
		ret += "\nstiffnessData:\n" + stiffnessData;
		ret += "\naudioData:\n" + audioData;

		ret += "\ncustom memory events:";
		foreach ( string key in customMemoryEvents.Keys )
		{
			ret += "\n\t" + key + ": " + customMemoryEvents[key];
		}

		return ret;
	}

}

