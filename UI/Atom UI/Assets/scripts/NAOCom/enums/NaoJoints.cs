﻿using System;

public enum NAOJoints
{
	Body,

	Head,
	HeadYaw,
	HeadPitch,

	LArm,
	LHand,	
	LLeg,

	RLeg,	
	RArm,
	RHand
}
