﻿using UnityEngine;
using System.Collections;

public class EV3InputTouch : AtomDisplayBase
{
	public enum TouchType
	{
		Sensor,
		Brick,
		Screen
	}

	void OnEnableCommon()
	{

	}

	public override void OnEnableEdit(AtomSettings atomSettings)
	{
		base.OnEnableEdit(atomSettings);

		OnEnableCommon();
	}

	public override void OnEnableRun(AtomSettings atomSettings)
	{
		base.OnEnableRun(atomSettings);

		OnEnableCommon();
	}
}
