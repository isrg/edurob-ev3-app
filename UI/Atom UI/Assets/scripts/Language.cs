﻿using System;
using UnityEngine.UI;
using UnityEngine;

public static class Language
{
	public enum AppLanguage
	{
		en,
		bg,
		it,
		lt,
		pl,
		tr
	}

	public static AppLanguage ActiveLanguage
	{
		get
		{
			var l = PlayerPrefs.GetString("ActiveLanguage", GetSystemLanguage());
			return LanguageString2Enum(l);
		}
		set
		{
			PlayerPrefs.SetString("ActiveLanguage", value.ToString());
			UIManager.LoadActiveLanguage();
		}
	}

	public static AppLanguage LanguageString2Enum(string name)
	{
		return (AppLanguage)Enum.Parse(typeof(AppLanguage), name, true);
	}
		
	public static Text UpdateLanguage(this Text value)
	{
		return value;
	}

	static string GetSystemLanguage()
	{
		switch (Application.systemLanguage)
		{
			case SystemLanguage.Bulgarian: return "bg";
			case SystemLanguage.Italian: return "it";
			case SystemLanguage.Polish: return "pl";
			case SystemLanguage.Lithuanian: return "lt";
			case SystemLanguage.Turkish: return "tr";
			default: return "en";
		}
	}
}

