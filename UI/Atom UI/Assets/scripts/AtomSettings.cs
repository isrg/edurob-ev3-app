﻿using UnityEngine;
using System;

[Serializable]
public class AtomSettings
{
	public string ID { get; set; }

	[SerializeField] public EV3ActionDrive EV3Drive;
	public struct EV3ActionDrive
	{
		[SerializeField] public int MotorSpeed;
		[SerializeField] public bool SliderVisible;
	}

	[SerializeField] public EV3ActionRunProgram EV3Program;
	public struct EV3ActionRunProgram
	{
		[SerializeField] public string ProgramName;
		[SerializeField] public string ProgramDescription;
	}

	public AtomSettings()
	{
		Debug.Log("new AtomSettings()");

		EV3Drive.MotorSpeed = 50;
		EV3Drive.SliderVisible = true;

		EV3Program.ProgramName = string.Empty;
		EV3Program.ProgramDescription = string.Empty;
	}

	public AtomSettings(AtomConfig atomConfig)
	{
		Debug.Log("new AtomSettings(" + atomConfig.AtomID +")");

		ID = atomConfig.AtomID;
		EV3Drive.MotorSpeed = 50;
		EV3Drive.SliderVisible = true;

		EV3Program.ProgramName = string.Empty;
		EV3Program.ProgramDescription = string.Empty;
	}

	public virtual void Run() {}

	public class RunMotor : AtomSettings
	{
		public RunMotor()
		{
			Defaults();
		}

		public void Defaults()
		{
		
		}

		public override void Run()
		{

		}
	}

//	public class Drive : AtomSettings
//	{
//		public OutputPort MotorLeft;
//		public OutputPort MotorRight;
//		public OutputPort MotorSpare;
//
//		public float MotorSpeed;
//		public bool SliderVisible;
//		public bool InvertMotors;
//
//		public string Test = "test string";
//
//		public Drive()
//		{
//			Defaults();
//		}
//
//		public void Defaults()
//		{
//			MotorLeft = OutputPort.B;
//			MotorRight = OutputPort.C;
//			MotorSpare = OutputPort.A;
//
//			MotorSpeed = 50;
//			SliderVisible = true;
//			InvertMotors = false;
//		}
//
//		public override void Setup()
//		{
//
//		}
//
//		public override void Run()
//		{
//			Debug.Log(GetType() + " activated");	
//		}
//	}
//
//	public class RunProgram : AtomSettings
//	{
//		public RunProgram()
//		{
//			Defaults();
//		}
//
//		public void Defaults()
//		{
//
//		}
//
//		public override void Run()
//		{
//
//		}
//	}

	public class AudioIn : AtomSettings
	{
		public AudioIn()
		{
			Defaults();
		}

		public void Defaults()
		{

		}

		public override void Run()
		{

		}
	}

	public class TouchIn : AtomSettings
	{
		public TouchIn()
		{
			Defaults();
		}

		public void Defaults()
		{

		}

		public override void Run()
		{

		}
	}

	public class VisionIn : AtomSettings
	{
		public VisionIn()
		{
			Defaults();
		}

		public void Defaults()
		{

		}

		public override void Run()
		{

		}
	}

	public class OtherIn : AtomSettings
	{
		public OtherIn()
		{
			Defaults();
		}

		public void Defaults()
		{

		}

		public override void Run()
		{

		}
	}

	public class AudioOut : AtomSettings
	{
		public AudioOut()
		{
			Defaults();
		}

		public void Defaults()
		{

		}

		public override void Run()
		{

		}
	}

	public class LightOut : AtomSettings
	{
		public LightOut()
		{
			Defaults();
		}

		public void Defaults()
		{

		}

		public override void Run()
		{

		}
	}

	public class OtherOut : AtomSettings
	{
		public OtherOut()
		{
			Defaults();
		}

		public void Defaults()
		{

		}

		public override void Run()
		{

		}
	}

	public class End : AtomSettings
	{
		public End()
		{
			Defaults();
		}

		public void Defaults()
		{

		}

		public override void Run()
		{

		}
	}
}
