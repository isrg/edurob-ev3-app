﻿using System;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public class CommandMessage : MonoBehaviour
{
	//http://docs.unity3d.com/Manual/UNetMessages.html


	NetworkClient myClient;

	public class NaoMsgType
	{
		public static short MyNaoMessage = MsgType.Highest + 1;
	}

	public class NaoCommand : MessageBase
	{
		public string Command;
	}

	public void SendMsg(string bhvr)
	{
		if (bhvr == string.Empty)
			bhvr = "Gangnam";

		const string CommandPrefix = "ALAutonomousLife.switchFocus(\"";
		const string CommandSuffix = "/.\")";

		NaoCommand msg = new NaoCommand();
		msg.Command = CommandPrefix + bhvr + CommandSuffix;

		NetworkServer.SendToAll(NaoMsgType.MyNaoMessage, msg);
	}

	public void SetupClient()
	{
		myClient = new NetworkClient();
		myClient.RegisterHandler(MsgType.Connect, OnConnected);
		myClient.RegisterHandler(NaoMsgType.MyNaoMessage, OnCommand);
	}

	void OnConnected(NetworkMessage netMsg)
	{
		Debug.Log("Connected to server");
	}

	void OnCommand(NetworkMessage netMsg)
	{
		NaoCommand msg = netMsg.ReadMessage<NaoCommand>();
		Debug.Log("received OnCommand " + msg.Command);
	}

}

public class NaoInstruction :NetworkBehaviour
{
	const short MyNaoInstruction = 1002;

	NetworkClient m_client;

	public void SendNaoInstruction(string bhvr)
	{
		if (bhvr == string.Empty)
			bhvr = "Gangnam";

		const string CommandPrefix = "ALAutonomousLife.switchFocus(\"";
		const string CommandSuffix = "/.\")";

		bhvr = CommandPrefix + bhvr + CommandSuffix;

		var msg = new StringMessage(bhvr);
		m_client.Send(MyNaoInstruction, msg);
	}

	public void Init(NetworkClient client)
	{
		m_client = client;
		NetworkServer.RegisterHandler(MyNaoInstruction, OnNaoInstructionResponse);
	}

	void OnNaoInstructionResponse(NetworkMessage netMsg)
	{
		var msg = netMsg.ReadMessage<StringMessage>();
		Debug.Log("received OnNaoInstructionResponse " + msg.value);
	}
}



