﻿//
//using UnityEngine;
//using System.Collections;
//using System.IO;
//
//
////===============================================================================================
//// TextManager
//// KLL 09/04/2009
////
//// Reads text files in the Assets\Resources\Languages directory into a Hashtable. The text file
//// consists of one line that has the key name while the next line has the actual
//// text to display.  This is used to localize a game.
////
////  Example:
////      Assume we have a text file called English.txt in Assets\Resources\Languages
////      The file looks like this:
//// 
////      HELLO
////      Hello and welcome!
////      GOODBYE
////      Goodbye and thanks for playing!
////
////      in code we file load the language file:
////          TextManager.LoadLanguage("English");
////
////      then we can retrieve text by calling
////          TextManager.GetText("HELLO");
////
////      will return a string containing "Hello and welcome!"
////
////===============================================================================================
//
//public class TextManager : MonoBehaviour
//{
//
//	// PRIVATE DECLARATIONS
//	private static TextManager _instance;
//	private static Hashtable textTable;
//	private TextManager () {}
//
//	//-----------------------------------------------------------------------------------------------
//	// Returns an instance of the TextManager
//	//-----------------------------------------------------------------------------------------------
//	private static TextManager Instance
//	{
//		get
//		{
//			if (_instance == null)
//			{
//				// Because the TextManager is a component, we have to create a GameObject to attach it to.
//				GameObject notificationObject = new GameObject("Default TextManager");
//
//				// Add the DynamicObjectManager component, and set it as the defaultCenter
//				_instance = (TextManager) notificationObject.AddComponent(typeof(TextManager));
//			}
//			return _instance;
//		}
//	}
//
//	//-----------------------------------------------------------------------------------------------
//	// Returns an instance of the TextManager
//	//-----------------------------------------------------------------------------------------------
//	public static TextManager GetInstance()
//	{
//		return Instance;
//	}  
//
//	public static bool LoadLanguage(string filename)
//	{
//		GetInstance();
//
//		string fullpath = "Languages/" +  filename;
//
//		TextAsset textAsset = (TextAsset) Resources.Load(fullpath, typeof(TextAsset));
//		if (textAsset == null)
//		{
//			Debug.Log(fullpath + " file not found.");
//			return false;
//		}
//
//		// create the text hash table if one doesn't exist
//		if (textTable == null)
//		{
//			textTable = new Hashtable();
//		}
//
//		// clear the hashtable
//		textTable.Clear();
//
//		StringReader reader = new StringReader(textAsset.text);
//		string key;
//		string val;
//		while(true)
//		{
//			key = reader.ReadLine();
//			val = reader.ReadLine();
//			if (key != null || val != null)
//			{
//				// TODO: add error handling here in case of duplicate keys
//				textTable.Add(key, val);
//			}
//			else
//			{
//				break;
//			}
//		}
//
//
//		reader.Close();
//
//		return true;
//	}
//
//
//	public static string GetText(string key)
//	{
//		// TODO: add error handling here in case the key doesn't exist
//		return  (string)textTable[key];
//	}
//}
