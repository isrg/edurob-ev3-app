﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class UIManager : MonoBehaviour
{
	public static UIManager Instance;

	public GameObject CommonScreenElements;

	public Text common_screen_title;
	public Text DebugStatus;

	public Button ContextualHelpButton;
	public UIMessage MessageOverlay;

	public UIScreenBase ViewSelectRobot;
	public UIScreenBase ViewMain;
	public UIScreenBase ViewConnect;
	public UIScreenBase ViewEV3Setup;
	public UIScreenBase ViewEdit;
	public UIScreenBase ViewRun;
	public UIScreenBase ViewLanguage;

	public UIScreenBase[] Views;

	public GameObject[] HideOnRun;

	UIScreenBase activeView;

	/// <summary>
	/// Mapping of each TextID to all UI Text objects that use it
	/// </summary>
	static Dictionary<string, Text[]> ID2Text = new Dictionary<string, Text[]>();
	/// <summary>
	/// Container for textIDs starting with "common_".
	/// These IDs refer to UIScreenBase.ViewText serializable class
	/// This is populated only when a new view is loaded.
	/// </summary>
	static List<UIScreenBase.ViewText> ViewTextItems = new List<UIScreenBase.ViewText>();
	/// <summary>
	/// Mapping of each TextID to its array of translations
	/// </summary>
	public static Dictionary<string, string[]> Translations = new Dictionary<string, string[]>();

	void Awake()
	{
		if (Instance != null)
			throw new Exception("An instance of " + this.GetType() + " already exists!");
		
		Instance = this;

		CommonScreenElements.SetActive(true);

		Views = GetComponentsInChildren<UIScreenBase>();

		ViewSelectRobot.Hide();
		ViewMain.Hide();
		ViewConnect.Hide();
		ViewEV3Setup.Hide();
		ViewEdit.Hide();
		ViewRun.Hide();
		ViewLanguage.Hide();

		LoadTranslations();
			
		GetTextObjects(gameObject);
		LoadActiveLanguage();

		foreach (var go in HideOnRun)
			go.SetActive(false);
	}

	void Start()
	{
		if (RobotManager.Selected == RobotType.None)
			activeView = ViewSelectRobot;
		else
			activeView = ViewMain;
		
		activeView.Show();
	}

	void Update()
	{
		if (Debug.isDebugBuild)
			DebugStatus.text = EV3Plugin.Instance.DebugStatus;
	}

	public static void ReturnToMainView()
	{
		Instance.SwitchViews(Instance.ViewMain);
	}

	public void SwitchViews(UIScreenBase screen)
	{
		activeView.Hide();
		activeView = screen;
		activeView.Show();
	}
		
	public static UIScreenBase GetActiveView()
	{
		return Instance.activeView;
	}

	void GetTextObjects(GameObject go)
	{
		var textComponent = go.GetComponents<Text>();

		if (textComponent.Length > 0 && ExpectedInDictionary(go.name))
		{
			if (ExpectedInDictionary(textComponent[0].text))
			{
				if (ID2Text.ContainsKey(go.name))
					ID2Text[go.name] = ID2Text[go.name].Concat(textComponent);
				else
					ID2Text.Add(go.name, textComponent);
			}
		}

		var uiComponent = go.GetComponent<UIScreenBase>();

		if (uiComponent != null)
			ViewTextItems.Add(uiComponent.viewText);

		foreach (Transform child in go.transform)
			GetTextObjects(child.gameObject);
	}

	public static void LoadActiveLanguage()
	{
		Debug.Log("UI Language set to: " + Language.ActiveLanguage);

		// Load current language strings into Text fields with textIDs
		foreach (var kvp in ID2Text)
		{
			var textID = kvp.Key;
			var textComponents = kvp.Value;
			try
			{
				foreach(var textItem in textComponents)
					textItem.text = GetTranslatedString(textID);
			}
			catch (KeyNotFoundException ex)
			{
				if (!textID.StartsWith("common_"))
					Debug.LogWarning(ex.Message + " - " + textID);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message + " - " + textID + "\n" + ex.StackTrace);
			}
		}

		// Load current language strings into UIScreenBase.ViewText for later display
		foreach (var viewText in ViewTextItems)
		{
			try
			{
				if (ExpectedInDictionary(viewText.ViewTitle))
					viewText.ViewTitle = GetTranslatedString(viewText.ViewTextIDs[0]);
				viewText.MessageTitle = GetTranslatedString(viewText.ViewTextIDs[1]);
				viewText.MessageContent = GetTranslatedString(viewText.ViewTextIDs[2]);
			}
			catch (KeyNotFoundException ex)
			{
				Debug.LogWarning(ex.Message + " - ViewText for " + viewText.ID + " in " + Language.ActiveLanguage);
			}
			catch (Exception ex)
			{
				Debug.LogError(ex.Message + " - ViewText for " + viewText.ID + " in " + Language.ActiveLanguage + "\n" + ex.StackTrace);
			}
		}
	}

	static bool ExpectedInDictionary(string str)
	{
		if (str.StartsWith("common_") || str == "set_at_runtime" || str == "blank" || str == "fixed_text")
			return false;

		return true;
	}

	static string GetTranslatedString(string textID)
	{
		var translation = Translations[textID][(int)Language.ActiveLanguage];
		return (translation == string.Empty) ? Translations[textID][0] : translation;
//		return (translation == string.Empty) ? textID : translation;
	}

	public static void LoadTranslations()
	{
		var textAsset = Resources.Load<TextAsset>("translated_strings");
		var cleanedStrings = textAsset.text.Replace("\r", "");
		var splitStrings = cleanedStrings.Split('\n');
		foreach(var line in splitStrings)
		{
			var stringArray = line.Split('\t');
			var TextID = stringArray[0];
			var translations = stringArray.Slice(1, 0);
			Translations.Add(TextID, translations);
		}
	}

	public static string GetText(string id)
	{
		string translation;

		try
		{
			translation = Translations[id][(int)Language.ActiveLanguage];
		}
		catch (KeyNotFoundException ex)
		{
			Debug.LogWarning(ex.Message + " - " + id);
			translation = id;
		}
		catch (Exception ex)
		{
			Debug.LogError(id + " - " + ex.Message + "\n" + ex.StackTrace);
			translation = id;
		}

		return translation;
	}
}
