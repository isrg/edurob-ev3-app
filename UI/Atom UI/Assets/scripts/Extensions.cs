﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Xml.Serialization;
using System.IO;
using System.Text;
using System.Xml;
using System.Linq.Expressions;

public static class Extensions
{
	public static bool IsIPv4(this string value)
	{
		var quads = value.Split('.');

		if (quads.Length != 4) return false;

		foreach(var quad in quads) 
		{
			int q;
			// if parse fails 
			// or length of parsed int != length of quad string (i.e.; '1' vs '001')
			// or parsed int < 0
			// or parsed int > 255
			// return false
			if (!Int32.TryParse(quad, out q) 
				|| !q.ToString().Length.Equals(quad.Length) 
				|| q < 0 
				|| q > 255) { return false; }

		}

		return true;
	}

	/// <summary>
	/// Get the array slice between the two indexes.
	/// ... Inclusive for start index, exclusive for end index.
	/// </summary>
	public static T[] Slice<T>(this T[] source, int start, int end)
	{
		// Handles negative ends.
		if (end <= 0)
		{
			end = source.Length + end;
		}
		int len = end - start;

		// Return new array.
		T[] res = new T[len];
		for (int i = 0; i < len; i++)
		{
			res[i] = source[i + start];
		}
		return res;
	}

	public static T[] Concat<T>(this T[] x, T[] y)
	{
		if (x == null) throw new ArgumentNullException("x");
		if (y == null) throw new ArgumentNullException("y");
		int oldLen = x.Length;
		Array.Resize<T>(ref x, x.Length + y.Length);
		Array.Copy(y, 0, x, oldLen, y.Length);
		return x;
	}

	public static T Deserialize<T>(string xml)
	{
		return Deserialize<T> (xml, new Type[]{});
	}

	public static T Deserialize<T>(string xml, Type[] extraTypes)
	{
		T obj = default(T);
		var serializer = new XmlSerializer(typeof(T), extraTypes);
		using (TextReader textReader = new StringReader(xml))
		obj = (T)serializer.Deserialize(textReader);
		return obj;
	}


	public static T DeserializeFromString<T>(string xml)
	{
		var xmlSerializer = new XmlSerializer(typeof(T));
		var obj = (T)xmlSerializer.Deserialize(new StringReader(xml));
		return obj;
	}

	public static string Serialize<T> (T obj)
	{
		return Serialize<T> (obj, new Type[]{});
	}

	public static string Serialize<T> (T obj, Type[] extraTypes)
	{
		var stringBuilder = new StringBuilder();
		var xmlSerializer = new XmlSerializer(typeof(T), extraTypes);
		using (TextWriter textWriter = new StringWriter(stringBuilder))
			xmlSerializer.Serialize(textWriter, obj);

		return stringBuilder.ToString();
	}
		
	public static string SerializeToString<T> (T obj)
	{
		return SerializeToString<T> (obj, new Type[]{});
	}

	public static string SerializeToString<T>(T obj, Type[] extraTypes)
	{
		var xmlSerializer = new XmlSerializer(typeof(T), extraTypes);
		var stringBuilder = new StringBuilder();
		xmlSerializer.Serialize(XmlWriter.Create(stringBuilder), obj);
		return stringBuilder.ToString();
	}

	public static string Serialize3<T> (T obj)
	{
		return Serialize3<T> (obj, new Type[]{});
	}

	public static string Serialize3<T>(T obj, Type[] extraTypes)
	{
		var xmlSerializer = new XmlSerializer(typeof(T));//, extraTypes);
		var stringWriter = new StringWriter();
		xmlSerializer.Serialize(stringWriter, obj);
		return stringWriter.ToString();
	}

	public static Type[] GetAllSubTypes(Type aBaseClass)
	{
		var subTypes = new List<Type>();
		Assembly[] assemblies = AppDomain.CurrentDomain.GetAssemblies();
		foreach (Assembly assembly in assemblies)
		{
			Type[] types = assembly.GetTypes();
			foreach (Type type in types)
			{
				if (type.IsSubclassOf(aBaseClass))
					subTypes.Add(type);
			}
		}
		return subTypes.ToArray();
	}
}

