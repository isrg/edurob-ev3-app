﻿using UnityEngine;
using System.Collections;
using System;

public class EV3ActionDummy : AtomDisplayBase
{
	void OnEnableCommon()
	{
		
	}

	public override void OnEnableEdit(AtomSettings atomSettings)
	{
		base.OnEnableEdit(atomSettings);

		OnEnableCommon();
	}

	public override void OnEnableRun(AtomSettings atomSettings)
	{
		base.OnEnableRun(atomSettings);

		OnEnableCommon();
	}
}
