﻿using UnityEngine;
using System;
using System.Collections.Generic;

[Serializable]
public class EV3 : Robot
{
	static EV3 _instance;

	public static EV3 Instance
	{
		get
		{
			if (_instance == null)
				_instance = new EV3(); //CreateInstance<EV3>();
			return _instance;
		}
	}

	public readonly new RobotType type = RobotType.EV3;
	public EV3Configuration EV3Config;

	public static string EV3Name
	{
		get { return PlayerPrefs.GetString(Tokens.PrefsEV3Name, "EV3"); }
		set
		{
			if (value == string.Empty)
				value = "EV3";

			PlayerPrefs.SetString(Tokens.PrefsEV3Name, value);
		}
	}

	public delegate void PortChangeEventHandler();
	public static event PortChangeEventHandler PortChangeEvent;

	EV3()
	{
		Debug.Log("EV3 instance created");

		EV3Plugin.BluetoothUpdateEvent += BluetoothUpdateEvent;
		EV3Plugin.PortDeviceUpdateEvent += PortDeviceUpdateEvent;
		PortChangeEvent += EV3_PortChangeEvent;
		_instance = this;
	}

	void EV3_PortChangeEvent ()
	{
		Debug.Log("==============> PortChangeEvent");
	}

	void BluetoothUpdateEvent (bool state)
	{
		Debug.Log("AtomUI Bluetooth state: " + state);
	}

	void PortDeviceUpdateEvent (InputPort port, Device device)
	{
		Debug.Log("AtomUI Peripheral detection: " + port + ", " + device);

		switch (port)
		{
			case InputPort.A: EV3State.MotorPorts[0] = device; break;
			case InputPort.B: EV3State.MotorPorts[1] = device; break;
			case InputPort.C: EV3State.MotorPorts[2] = device; break;
			case InputPort.D: EV3State.MotorPorts[3] = device; break;
			case InputPort.One: EV3State.SensorPorts[0] = device; break;
			case InputPort.Two: EV3State.SensorPorts[1] = device; break;
			case InputPort.Three: EV3State.SensorPorts[2] = device; break;
			case InputPort.Four: EV3State.SensorPorts[3] = device; break;
		}

		PortChangeEvent();
	}
		
	~EV3()
	{
		EV3Plugin.BluetoothUpdateEvent -= BluetoothUpdateEvent;
		EV3Plugin.PortDeviceUpdateEvent -= PortDeviceUpdateEvent;

		PortChangeEvent-= EV3_PortChangeEvent;
	}
		
	public void LoadConfig()
	{
		EV3Config = LoadFromPrefs(EV3Name);
	}

	public void SaveConfig()
	{
		SaveToPrefs(EV3Name, EV3Config);
	}

	public static EV3Configuration LoadFromPrefs(string EV3Name)
	{
		var ev3ConfigString = PlayerPrefs.GetString(EV3Name + "_config", "empty");
		Debug.Log("Load " + EV3Name + "_config : " + ev3ConfigString);
		if (ev3ConfigString == "empty")
		{
			var EV3Config = new EV3Configuration();
			SaveToPrefs(EV3Name, EV3Config);
			return EV3Config;
		}
		var scenarioSettings = Extensions.Deserialize<EV3Configuration>(ev3ConfigString);
		return scenarioSettings;
	}

	public static void SaveToPrefs(string EV3Name, EV3Configuration ev3ConfigString)
	{
		var save = Extensions.Serialize<EV3Configuration>(ev3ConfigString);
		Debug.Log("Save " + EV3Name + "_config : " + save);
		PlayerPrefs.SetString(EV3Name + "_config", save);
	}

	public static readonly InputPort[] MotorPortMap = {InputPort.A, InputPort.B, InputPort.C, InputPort.D};
	public static readonly InputPort[] SensorPortMap = {InputPort.One, InputPort.Two, InputPort.Three, InputPort.Four};

	public static InputPort[] GetMotorConfig()
	{
		var motors = new List<KeyValuePair<InputPort, Device>>();

		for (int i = 0; i < EV3State.MotorPorts.Count; i++)
		{
			var device = EV3State.MotorPorts[i];
			var port = EV3.MotorPortMap[i];
			motors.Add(new KeyValuePair<InputPort, Device>(port, device));
		}

		var LMotors = motors.FindAll(kvp => kvp.Value == Device.LMotor);

		var driveMotorPorts = new [] {InputPort.Null, InputPort.Null};

		for (int i=0; i < Mathf.Min(LMotors.Count, driveMotorPorts.Length); i++)
		{
			driveMotorPorts[i] = LMotors[i].Key;
		}

		if (EV3.Instance.EV3Config.SwapMotors)
			Array.Reverse(driveMotorPorts);

		Debug.Log("Motors: " + driveMotorPorts[0] + " - " + driveMotorPorts[1]);
		return driveMotorPorts;
	}
}

[Serializable]
public class EV3Configuration
{
	public List<string> ProgramPaths = new List<string>();

//	public InputPort LMotorPort = InputPort.Null;
//	public InputPort RMotorPort = InputPort.Null;
//	public InputPort SMotorPort = InputPort.Null;
//
	public bool InvertMotors;
	public bool SwapMotors;
}

public static class EV3State
{
	public static Device PortA = Device.Empty;
	public static Device PortB = Device.Empty;
	public static Device PortC = Device.Empty;
	public static Device PortD = Device.Empty;
	public static Device Port1 = Device.Empty;
	public static Device Port2 = Device.Empty;
	public static Device Port3 = Device.Empty;
	public static Device Port4 = Device.Empty;

	public static List<Device> MotorPorts = new List<Device> {PortA, PortB, PortC, PortD};
	public static List<Device> SensorPorts = new List<Device> {Port1, Port2, Port3, Port4};
}