using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class Scenario
{
//	private Guid GUID;
	public string ScenarioID { get; private set; }
	public string ScenarioName { get; set; }

	public List<AtomSettings> AtomSettingsList;

	public Scenario()
	{
		var GUID = Guid.NewGuid();
		ScenarioID = GUID.ToString();
		ScenarioName = string.Empty;
	}

	public Scenario (KeyValPair<string, string> scenarioKVP)
	{
		ScenarioID = scenarioKVP.Key;
		ScenarioName = scenarioKVP.Value;
	}
}

