﻿using UnityEngine.UI;

public class UIViewLanguage : UIScreenBase
{
	protected override void Awake()
	{
		base.Awake();

		foreach (var bttn in GetComponentsInChildren<Button>())
		{
			var selectedLanguage = Language.LanguageString2Enum(bttn.name);
			bttn.onClick.AddListener(() => {
				Language.ActiveLanguage = selectedLanguage;
				UIManager.ReturnToMainView();

				// var uiManager = gameObject.transform.root.gameObject.GetComponent<UIManager>();
				// uiManager.SwitchScreens(uiManager.Main);
			});
		}
	}

	protected override void Start()
	{
		base.Start();
			}

	protected override void OnEnable()
	{
		base.OnEnable();
	}

	protected override void Update()
	{
		base.Update();
	}
}
