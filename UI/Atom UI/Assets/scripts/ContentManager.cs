﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class ContentManager : MonoBehaviour
{
	public static ContentManager Instance;
	public static Scenario ActiveScenario;
	public static Dictionary<string, AtomConfig> AtomLookup;
	public static List<KeyValPair<string, string>> DefaultList;
	public static List<KeyValPair<string, string>> SaveList;

	static KeyValPair<string, string> ActiveKVP;

	public static int NumberOfSaves;
	public List<TextAsset> TemplateScenarios;
	public List<AtomConfig> AtomConfigurations;

	[NonSerialized] public static bool EditingActive;
//	public AtomDefinition BaseAtom = new AtomDefinition("add_new", new [] {"add_action", "add_output", "add_input"});
//	public AtomDefinition RobotAction = new AtomDefinition("add_action", new [] {"add_move", "run_program"});
//	public AtomDefinition RobotActionMove = new AtomDefinition("add_move", new [] {"run_motor", "drive_robot"});
//	public AtomDefinition RobotActionRunMotor = new AtomDefinition("run_motor", null);
//	public AtomDefinition RobotActionDrive = new AtomDefinition("drive_robot", null);
//	public AtomDefinition RobotActionProgram = new AtomDefinition("run_program", null);
//	public AtomDefinition RobotOutput = new AtomDefinition("add_output", new [] {"output_audio", "output_light", "output_other"});
//	public AtomDefinition RobotOutputAudio = new AtomDefinition("output_audio", null);
//	public AtomDefinition RobotOutputLight = new AtomDefinition("output_light", null);
//	public AtomDefinition RobotOutputOther = new AtomDefinition("output_other", null);
//	public AtomDefinition RobotInput = new AtomDefinition("add_input", new [] {"input_touch", "input_audio", "input_visual", "input_other"});
//	public AtomDefinition RobotInputTouch = new AtomDefinition("input_touch", null);
//	public AtomDefinition RobotInputAudio = new AtomDefinition("input_audio", null);
//	public AtomDefinition RobotInputVision = new AtomDefinition("input_visual", null);
//	public AtomDefinition RobotInputOther = new AtomDefinition("input_other", null);
//	public AtomDefinition EndScenario = new AtomDefinition("end_scenario", new [] {"add_new", "end_scenario"});

	void Awake()
	{
		Instance = this;

		AtomLookup = new Dictionary<string, AtomConfig>();
		foreach (var config in ContentManager.Instance.AtomConfigurations)
			AtomLookup.Add(config.AtomID, config);
		
//		AtomDefinitions = new List<AtomDefinition>();
//
//		AtomDefinitions.Add(BaseAtom);
//		AtomDefinitions.Add(RobotAction);
//		AtomDefinitions.Add(RobotActionMove);
//		AtomDefinitions.Add(RobotActionRunMotor);
//		AtomDefinitions.Add(RobotActionDrive);
//		AtomDefinitions.Add(RobotActionProgram);
//		AtomDefinitions.Add(RobotOutput);
//		AtomDefinitions.Add(RobotOutputAudio);
//		AtomDefinitions.Add(RobotOutputLight);
//		AtomDefinitions.Add(RobotOutputOther);
//		AtomDefinitions.Add(RobotInput);
//		AtomDefinitions.Add(RobotInputTouch);
//		AtomDefinitions.Add(RobotInputAudio);
//		AtomDefinitions.Add(RobotInputVision);
//		AtomDefinitions.Add(RobotInputOther);
//		AtomDefinitions.Add(EndScenario);
		DefaultList = new List<KeyValPair<string, string>>();

		foreach (var ta in TemplateScenarios)
		{
			var kvp = new KeyValPair<string,string>("Template", ta.name);
			DefaultList.Add(kvp);
		}
	
		var savedSenarios = PlayerPrefs.GetString(Tokens.PrefsEV3ScenarioList, string.Empty);

		if (savedSenarios == string.Empty)
			SaveList = new List<KeyValPair<string, string>>();
		else
			SaveList = Extensions.Deserialize<List<KeyValPair<string, string>>>(savedSenarios);

		NumberOfSaves = SaveList.Count;

		for (int i = 0; i < SaveList.Count; i++)
			Debug.Log("Existing saves: " + (i+1) + " : " + SaveList[i].Key + " - " + SaveList[i].Value);
	}

	void Update()
	{
		NumberOfSaves = SaveList.Count;
	}

	public static void UpdateScenarioReference()
	{
		if (ActiveKVP != null)
			ActiveKVP.Value = ActiveScenario.ScenarioName;
		
		var savedScenarios = Extensions.Serialize<List<KeyValPair<string, string>>>(SaveList);
		PlayerPrefs.SetString(Tokens.PrefsEV3ScenarioList, savedScenarios);

		Debug.Log("UpdateScenarioReference : " + savedScenarios);
	}

	public static void NewScenario()
	{
		ActiveScenario = new Scenario();

		ActiveKVP = new KeyValPair<string, string>();
		ActiveKVP.Key = ActiveScenario.ScenarioID;
		ActiveKVP.Value = string.Empty;
		SaveList.Add(ActiveKVP);
		UpdateScenarioReference();
	}

	public static void EditScenario(KeyValPair<string, string> scenarioKVP)
	{
		if (scenarioKVP.Key == "Template")
		{
			ActiveKVP = scenarioKVP;
			var content = GetTemplateScenario();
			NewScenario();
			ActiveScenario.AtomSettingsList = content;

			ActiveKVP.Value = scenarioKVP.Value + " (copy)";
			ActiveScenario.ScenarioName = ActiveKVP.Value;
		}
		else
			LoadScenario(scenarioKVP);
	}

	public static void LoadScenario(KeyValPair<string, string> scenarioKVP)
	{
		ActiveKVP = scenarioKVP;

		Debug.Log("Loading:  " + ActiveKVP.Value + " (" + ActiveKVP.Key + ")");

		ActiveScenario = new Scenario(ActiveKVP);

		if (ActiveKVP.Key == "Template")
			ActiveScenario.AtomSettingsList = GetTemplateScenario();
		else
			ActiveScenario.AtomSettingsList = GetSavedScenario();
		
//		ActiveScenario.ScenarioName = ActiveKVP.Value;

		Debug.Log("Loaded " + ActiveScenario.ScenarioName + " (" + ActiveScenario.ScenarioID + ")");
	}

	static List<AtomSettings> GetTemplateScenario()
	{
		var path = "template_scenarios/" + ActiveKVP.Value;
		var textAsset = Resources.Load<TextAsset>(path);
		var xml = textAsset.ToString();
		return ConvertXmlToScenario(xml);
	}

	static List<AtomSettings> GetSavedScenario()
	{
		var xml = PlayerPrefs.GetString(ActiveKVP.Key);
		return ConvertXmlToScenario(xml);
	}

	static List<AtomSettings> ConvertXmlToScenario(string xml)
	{
		Debug.Log(xml);

		Type[] extraTypes = Extensions.GetAllSubTypes(typeof(AtomSettings));
		var content = Extensions.Deserialize<List<AtomSettings>>(xml, extraTypes);
		content.TrimExcess();
		return content;
	}

	public static void SaveScenario()
	{
		Type[] extraTypes = Extensions.GetAllSubTypes(typeof(AtomSettings));
		var xml = Extensions.Serialize<List<AtomSettings>>(ActiveScenario.AtomSettingsList, extraTypes);
		PlayerPrefs.SetString(ActiveScenario.ScenarioID, xml);
		Debug.Log("Saved " + ActiveScenario.ScenarioID + "\n" + xml);
	}

	public void DeleteScenario()
	{
		PlayerPrefs.DeleteKey(ActiveKVP.Key);
		SaveList.Remove(ActiveKVP);
		ActiveScenario = null;
		ActiveKVP = null;
		UpdateScenarioReference();
	}

	public static void ClearAllPrefs()
	{
		PlayerPrefs.DeleteAll();
		SaveList = new List<KeyValPair<string, string>>();
	}
}

//[Serializable]
//public struct KeyValPair<TKey,TValue>
//{
//	public TKey Key {get;set;}
//	public TValue Value {get;set;}
//}
	
[Serializable]
public class KeyValPair<TKey, TValue>
{
	public TKey Key { get; set; }
	public TValue Value { get; set; }

	public KeyValPair()
	{
	}

	public KeyValPair(TKey key, TValue value)
	{
		Key = key;
		Value = value;
	}
}
