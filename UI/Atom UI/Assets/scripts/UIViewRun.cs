﻿using UnityEngine;
using UnityEngine.UI;

public class UIViewRun : UIScreenBase
{
	public Button BackButton;
	public Button PlayPauseButton;
	public Sprite PlaySprite;
	public Sprite PauseSprite;
	public GameObject Stage;

	AtomSettings atomSettings;

	bool isPaused;

	protected override void Start()
	{
		base.Start();
	}

	protected override void OnEnable()
	{
		ContentManager.EditingActive = false;

		var title = ContentManager.ActiveScenario.ScenarioName;
		Debug.Log("Loading " + title);
		viewText.ViewTitle = title;

//		viewText.ViewTitle = "Scenario Title Here";
//		viewText.MessageTitle = "Customisable message";
//		viewText.MessageContent = "A bit of support info written by the trainer here";

		isPaused = false;
		base.OnEnable();
		UpdatePauseState();

		DisplayActivity(0);
	}

	void DisplayActivity(int i)
	{
		ResetStage();

		atomSettings = ContentManager.ActiveScenario.AtomSettingsList[i];
		var atomID = atomSettings.ID;
		var atomDefinition = ContentManager.AtomLookup[atomID];
		var clone = Instantiate(atomDefinition.Prefab);
		clone.transform.SetParent(Stage.transform, false);

		var atomDisplay = clone.GetComponent<AtomDisplayBase>();
		atomDisplay.OnEnableRun(atomSettings);
	}

	protected override void Update()
	{
		base.Update();
	}

	public void PlayPause()
	{
		isPaused = !isPaused;
		UpdatePauseState();
	}

	void UpdatePauseState()
	{
		Debug.Log("Update pause state to " + isPaused);
		BackButton.interactable = isPaused;
		PlayPauseButton.image.sprite = isPaused ? PlaySprite : PauseSprite;
	}

	void ResetStage()
	{
		foreach (Transform child in Stage.transform)
			Destroy(child.gameObject);
	}
}
