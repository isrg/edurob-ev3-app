﻿using UnityEngine;
using System;

public abstract class UIScreenBase: MonoBehaviour
{
	public ViewText viewText;

	protected virtual void Awake()
	{
		
	}

	protected virtual void Start()
	{
		
	}

	protected virtual void Update()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			Debug.Log("Input.GetKey(KeyCode.Escape)");
		}
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			Debug.Log("Input.GetKeyDown(KeyCode.Escape)");
			Exit();
		}
	}

	protected virtual void OnEnable()
	{
		SetTitle();
	}

	protected virtual void OnDisable()
	{

	}

	public void SetTitle()
	{
		UIManager.Instance.common_screen_title.text = viewText.ViewTitle;
	}

	public void SetTitle(String titleText)
	{
		UIManager.Instance.common_screen_title.text = titleText;
	}

	public void Hide()
	{
		gameObject.SetActive(false);
	}

	public void Show()
	{
		gameObject.SetActive(true);
	}

	void Exit()
	{
		Application.Quit();
	}

	[Serializable]
	public class ViewText
	{
		public string ID;
		public string ViewTitle;
		public string MessageTitle;
		[TextArea(1,10)] public string MessageContent;

		public string[] ViewTextIDs
		{
			get
			{
				return new[] {ID + "_view_title", ID + "_msg_title", ID + "_msg_content"};
			}
		}
			
		public string[] ViewTextItems
		{
			get
			{
				return  new[] {ViewTitle, MessageTitle, MessageContent};
			}
		}
	}
}
