﻿using UnityEngine;
using UnityEngine.UI;

public class UIViewConnect : UIScreenBase
{
	public GameObject SelectPanel;
	public GameObject BTPanel;
	public GameObject WifiPanel;

	GameObject activePanel;

	ViewText defaultViewText;

	protected override void Start()
	{
		base.Start();
		defaultViewText = viewText;
	}
	
	protected override void OnEnable()
	{
		base.OnEnable();

		var robot = RobotManager.Instance.ActiveRobot;

		if (robot.BlueTooth && robot.Wifi)
			DisplaySelect();
		else if (robot.BlueTooth)
			UseBluetooth();
		else
			UseWifi();
	}

	protected override void Update()
	{
		base.Update();
	}

	void DisplaySelect()
	{
		viewText = defaultViewText;
		SwitchPanels(SelectPanel);
	}

	public void UseBluetooth()
	{
		viewText = BTPanel.GetComponent<UIPanelBluetooth>().viewText;
		SwitchPanels(BTPanel);
	}

	public void UseWifi()
	{
		viewText = BTPanel.GetComponent<UIPanelWifi>().viewText;
		SwitchPanels(WifiPanel);
	}

	void SwitchPanels(GameObject panel)
	{
		if (null != activePanel)
			activePanel.SetActive(false);
		
		activePanel = panel;
		SetTitle();
		activePanel.SetActive(true);
	}
}
