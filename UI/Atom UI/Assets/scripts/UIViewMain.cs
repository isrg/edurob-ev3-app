﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class UIViewMain : UIScreenBase
{
	public Image RobotImage;

	public Button ScenarioButton;
	public GameObject ScrollViewContent;

	public Button ConnectionButtton;
	public Sprite ConnectedSprite;
	public Sprite DisconnectedSprite;

	public Button EditLockButton;
	public Sprite LockedSprite;
	public Sprite UnlockedSprite;

	bool IsLocked = true;

	bool connectionButtonState;

	bool started;

	protected override void Awake()
	{
	}

	protected override void Start()
	{
		base.Start();

		LoadScreen();
		started = true;
	}

	protected override void OnEnable()
	{
		if (started)
			LoadScreen();
	}

	void LoadScreen()
	{
		UpdateConnectionButtonState();

		if (!PlayerPrefs.HasKey(Tokens.PrefsEV3Name))
		{
			UIManager.Instance.SwitchViews(UIManager.Instance.ViewConnect);
			return;
		}

		if (!PlayerPrefs.HasKey(EV3.EV3Name + "_config"))
		{
			UIManager.Instance.SwitchViews(UIManager.Instance.ViewEV3Setup);
			return;
		}

		if (RobotManager.Instance.ActiveRobot.BlueTooth && (PlayerPrefs.GetString(Tokens.PrefsEV3Name) != string.Empty))
			EV3Plugin.Instance.InitialisePlugin();

		EV3.Instance.LoadConfig();

		var r = RobotManager.Instance.ActiveRobot;
		RobotImage.sprite = r.picture;

//		SendMessage("OnCreateList");

		foreach (Button staleButton in ScrollViewContent.GetComponentsInChildren<Button>())
			Destroy(staleButton.gameObject);

		foreach (var item in ContentManager.DefaultList)
			AddListItems(item, true);
		
		foreach (var item in ContentManager.SaveList)
			AddListItems(item, false);

		base.OnEnable();
	}

	void AddListItems(KeyValPair<string, string> item, bool isDefault)
	{
		var scenario = item;
		Button bttn = Instantiate(ScenarioButton);
		bttn.transform.SetParent(ScrollViewContent.transform);
		bttn.transform.localScale = Vector3.one;
		bttn.onClick.AddListener(delegate {
			LoadScenario(scenario);
		});
		//		bttn.onClick.AddListener(LoadScenario);
		//		bttn.onClick.AddListener(delegate { LoadScenario(msg); });
		//		bttn.onClick.AddListener(() => LoadScenario(msg));
		var bttnScript = bttn.GetComponent<UIListButton>();
		bttnScript.cellLabel.text = item.Value;
		bttnScript.statusImage.enabled = isDefault;

		Debug.Log("Scenario: " + item.Value + " (" + item.Key + ")");
	}

	protected override void Update()
	{
		base.Update();

		if (EV3Plugin.Instance.BluetoothIsConnected != connectionButtonState)
			UpdateConnectionButtonState();
	}

	void UpdateConnectionButtonState()
	{
		if (EV3Plugin.Instance.BluetoothIsConnected)
		{
			ConnectionButtton.image.sprite = ConnectedSprite;
			ConnectionButtton.GetComponentInChildren<Text>().text = UIManager.GetText("state_connected");
		}
		else
		{
			ConnectionButtton.image.sprite = DisconnectedSprite;
			ConnectionButtton.GetComponentInChildren<Text>().text = UIManager.GetText("state_disconnected");
		}
		connectionButtonState = EV3Plugin.Instance.BluetoothIsConnected;
	}

	public void ConfigureRobot()
	{
		if (RobotManager.Instance.ActiveRobot.type == RobotType.EV3)
		{
			UIManager.Instance.SwitchViews(UIManager.Instance.ViewEV3Setup);
		}
	}

	public void LoadScenario(KeyValPair<string, string> scenarioKVP)
	{
		if (IsLocked)
			RunScenarion(scenarioKVP);
		else
			EditScenario(scenarioKVP);
	}

	void RunScenarion(KeyValPair<string, string> scenarioKVP)
	{
		ContentManager.LoadScenario(scenarioKVP);

		UIManager.Instance.SwitchViews(UIManager.Instance.ViewRun);
	}

	public void ToggleEditLock()
	{
		IsLocked = !IsLocked;

		EditLockButton.image.sprite = IsLocked ? LockedSprite : UnlockedSprite;
	}

	public void EditScenario(KeyValPair<string, string> scenarioKVP)
	{
		ContentManager.EditScenario(scenarioKVP);

		var editView = (UIViewEdit) UIManager.Instance.ViewEdit;
		editView.IsNewScenario = false;

		UIManager.Instance.SwitchViews(UIManager.Instance.ViewEdit);
	}

	public void NewScenario()
	{
		var editView = (UIViewEdit) UIManager.Instance.ViewEdit;
		editView.IsNewScenario = true;

		ContentManager.NewScenario();
		UIManager.Instance.SwitchViews(UIManager.Instance.ViewEdit);
	}

	public void ClearAllPrefs()
	{
		ContentManager.ClearAllPrefs();
		UIManager.Instance.SwitchViews(UIManager.Instance.ViewSelectRobot);
	}
}
