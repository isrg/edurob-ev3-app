﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class UIMessage : MonoBehaviour
{	
	static UIMessage _instance;

	public static UIMessage Instance
	{
		get
		{
			if (_instance == null)
			{
				var instanceList = Resources.FindObjectsOfTypeAll<UIMessage>();

				if (instanceList.Length > 1)
					Debug.LogWarning("There is more than one " + instanceList.GetType() + " in this scene! The first one found will be used");
				else if (instanceList.Length == 0)
					throw new Exception ("Cannot find any objects of type " + instanceList.GetType() + " in the scene. Do you need to add one?");
				
				_instance = instanceList[0];
			}

			return _instance;
		}
	}

	public Text MessageTitle;
	public Text MessageContent;

	string defaultTitle = "Message";

	public static void Alert(string title, string message)
	{
		Instance.gameObject.SetActive(true);
		Instance.MessageTitle.text = title;
		Instance.MessageContent.text = message;
	}

	public static void Alert(string message)
	{
		Instance.gameObject.SetActive(true);
		Instance.MessageTitle.text = _instance.defaultTitle;
		Instance.MessageContent.text = message;
	}

	void OnEnable()
	{
		var currentScreen = UIManager.GetActiveView();
		Instance.MessageTitle.text = currentScreen.viewText.MessageTitle;
		Instance.MessageContent.text = currentScreen.viewText.MessageContent.Replace("\\n", "\n");
	}
}
