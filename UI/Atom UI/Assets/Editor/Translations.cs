﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;

public class Translations : EditorWindow
{
	bool translationsLoaded;
	Vector2 scrollPos;

	void OnGUI()
	{
		if (GUILayout.Button("Load"))
		{
			UIManager.Translations = new Dictionary<string, string[]>();
			UIManager.LoadTranslations();

			translationsLoaded = true;
		}

		if (translationsLoaded)
		{
			scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
			foreach(KeyValuePair<string, string[]> pair in UIManager.Translations)
			{
				try
				{
					Debug.Log(pair.Key);

					EditorGUILayout.BeginHorizontal();
					{	
						EditorGUILayout.LabelField(pair.Key);
						EditorGUILayout.TextField(pair.Value[0]);
					}
					EditorGUILayout.EndHorizontal();
				}
				catch (Exception e)
				{
					Debug.LogWarning(e.Message);
				}
			}
			EditorGUILayout.EndScrollView();
		}
	}

	void OnDisable()
	{
		this.Close();
	}
}
