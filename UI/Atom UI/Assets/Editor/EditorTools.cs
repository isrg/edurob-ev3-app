﻿using UnityEngine;
using UnityEditor;
using UnityEngine.UI;
using System.Collections.Generic;
using System.IO;
using System;

public class EditorTools
{
	static List<string> TextIDs;
	static bool IdentifyMissingItems;

	[MenuItem ("Edurob/Clear PlayerPrefs")]
	public static void  ClearPlayerPrefs()
	{
		PlayerPrefs.DeleteAll();
	}

	static readonly string stringPath = Application.dataPath + "/Resources/";

	[MenuItem ("Edurob/Show language window")]
	public static void  ShowWindow()
	{
		EditorWindow.GetWindow<Translations>();
	}

	[MenuItem ("Edurob/Extract english text to file")]
	public static void  ExtractEnglishText()
	{
		IdentifyMissingItems = false;
		UIManager.Translations = null;
		TextIDs = new List<string>();
		ExtractText();
	}

	[MenuItem ("Edurob/Extract all text to file")]
	public static void  ExtractAllText()
	{
		IdentifyMissingItems = false;
		UIManager.Translations = new Dictionary<string, string[]>();
		UIManager.LoadTranslations();
		TextIDs = new List<string>();
		ExtractText();
	}

	[MenuItem ("Edurob/Extract missing dictionary entries to file")]
	public static void  ExtractMissingText()
	{
		IdentifyMissingItems = true;
		UIManager.Translations = new Dictionary<string, string[]>();
		UIManager.LoadTranslations();
		TextIDs = new List<string>();
		ExtractText();
	}

	[MenuItem ("Edurob/Save current scenario as a resource")]
	public static void SaveConfigAsTemplate()
	{
		Debug.Log("Writing " + ContentManager.ActiveScenario.ScenarioName + " to the Resources folder");
		Type[] extraTypes = Extensions.GetAllSubTypes(typeof(AtomSettings));
		var s = ContentManager.ActiveScenario.AtomSettingsList;
		var id = ContentManager.ActiveScenario.ScenarioName;
		var save = Extensions.Serialize<List<AtomSettings>>(s, extraTypes);
		string path = stringPath + "template_scenarios/" + id + ".xml";
		var writer = new StreamWriter(path);
		writer.Write(save);
		writer.Close();
	}

	static void ExtractText()
	{
		var selected = Selection.gameObjects;
		if (selected == null || selected.Length == 0)
		{
			Debug.LogError("Nothing selected. Select the UI Canvas containing text to be acquired.");
			return;
		}

		var defaultName = "exported_strings" + (UIManager.Translations != null ? "_all" : "_en");
		if (IdentifyMissingItems)
			defaultName = "missing_strings";

		var filePath = EditorUtility.SaveFilePanel("Save text strings (tab separated)", stringPath, defaultName, "txt");

		using (var writer = new StreamWriter(filePath, false, System.Text.Encoding.UTF8))
		{
			writer.WriteLine("Text ID" + "\t" + "en" + "\t" + "bg" + "\t" + "it" + "\t" + "lt" + "\t" + "pl" + "\t" + "tr");

			foreach (GameObject go in selected)
				GetTextContent(go.transform.root.gameObject, writer);
		}

		Debug.Log("Text wriiten to: " + filePath);
	}

	static void GetTextContent(GameObject go, StreamWriter writer)
	{
		var textComponent = go.GetComponent<Text>();

		if (textComponent != null)
			WriteToFile(go.name, textComponent.text, writer);

		var UIComponent = go.GetComponent<UIScreenBase>();

		if (UIComponent != null)
		{
			UIScreenBase.ViewText viewText = UIComponent.viewText;
			for (var i = 0; i < viewText.ViewTextIDs.Length; i++)
			{
				WriteToFile(viewText.ViewTextIDs[i], viewText.ViewTextItems[i], writer);
			}
		}

		foreach (Transform child in go.transform)
			GetTextContent(child.gameObject, writer);
	}

	static void WriteToFile(string ID, string newText, StreamWriter writer)
	{
		if (newText == "set_at_runtime" || newText == "blank" || ID == "fixed_text")
		{
			Debug.Log("Skipping " + ID + " (" + newText + ")");
			return;
		}
		
		if (TextIDs.Contains(ID))
			return;
		else
			TextIDs.Add(ID);
		
		var txt = "\t" + newText;

		try
		{
			if (UIManager.Translations != null)
			{
				var line = UIManager.Translations[ID];

				txt = string.Empty;

				foreach(string s in line)
					txt += "\t\"" + s + '"';

				if (line[0] != newText)
					txt += "\t" + newText;
			}
			if (IdentifyMissingItems)
				return;
		}
		catch (Exception ex)
		{
			Debug.LogWarning(ID + " (" + newText + ") " + ex.Message);
		}

		writer.WriteLine(ID + txt);
	}

//	[MenuItem ("Edurob/Import l10n text from csv")]
//	public static void  ImporttText()
//	{
//		var path = EditorUtility.OpenFilePanelWithFilters("Locate localised content", stringPath, new[]{"Tab separated translation table", "txt"});
//		using (var reader = new StreamReader(path, System.Text.Encoding.UTF8))
//		{
//			var s = reader.ReadLine();
//			string langs = s.Substring(s.IndexOf("\t"));
//			Debug.Log("Languages present: " + langs);
//		}
//	}
}
