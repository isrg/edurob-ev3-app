# EDUROB EV3 Controller #
This is the Unity C# code for the EDUROB ([edurob.eu](http://edurob.eu)) project's Robot Controller app. It allows simple control of a Lego Mindstorm EV3.

### What is this repository for? ###
* This repository hosts the development code for the [EDUROB Robot Controller app](https://play.google.com/store/apps/details?id=uk.org.isrg.edurob.robotcontroller)
* The current release version is 0.2.1

### How do I get set up? ###

#### Prerequisites ####
* [Unity](http://unity3d.com/get-unity)
* [Android SDK](https://developer.android.com/studio/) (You only need the SDK tools package, not Android Studio)
* [Java JDK](http://www.oracle.com/technetwork/java/javase/downloads/) - (You'd probably be OK with [Open JDK](http://openjdk.java.net) on Linux)

#### Configuration ####
* The Unity IDE needs paths to the Android SDK and the JDK setting in *Edit > Preferences > External Tools*
* Plug an Android device with Developer Tools enabled into your development machine, and ensure that **Android** is the selected platform in *File > Build Settings*

#### Additional EV3 Program Files Used in the Project ###
* EV3 Scenario programs http://isrg.org.uk/robotev3/

# CREDITS #
This project (543577-LLP-1-2013-1-UK-KA3-KA3MP) has been funded with support from the European Commission.
This publication reflects the views only of the author, and the Commission cannot be held responsible for any use which may be made of the information contained therein.

# LICENSING #
![88x31.png](https://bitbucket.org/repo/M4EqLB/images/1763262374-88x31.png)

The Edurob Robot Controller by The Edurob Project Consortium is licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

Permissions beyond the scope of this license may be available at http://edurob.eu.